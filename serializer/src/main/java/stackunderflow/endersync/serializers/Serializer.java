package stackunderflow.endersync.serializers;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;


/*
 * Manages serialization setup.
 */
@Getter
@Setter
public class Serializer {

    // ================================     VARS

    // References
    public static Serializer INSTANCE;
    private ItemStackSerializer itemStackSerializer;
    private InventorySerializer inventorySerializer;
    private PotionEffectSerializer potionEffectSerializer;
    private GenericSerializer genericSerializer;

    // Settings.
    private String version;




    // ================================     CONSTRUCTOR

    public Serializer() {
        Serializer.INSTANCE = this;

        // Check version.
        try {
            setVersion(Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3]);
        } catch (ArrayIndexOutOfBoundsException e) {
            Bukkit.getConsoleSender().sendMessage("§cCould not initialize Serializer (could not get version)!");
            return;
        }

    }


    /*
     * Initializes the Serializer.
     */
    public void init() {

        if (!isCompatibleVersion()) {
            Bukkit.getConsoleSender().sendMessage("§cCould not initialize Serializer (incompatible version)! This should have been detected earlier!");
            return;
        }

        // Set universal serializers.
        setInventorySerializer(new InventorySerializer());
        setPotionEffectSerializer(new PotionEffectSerializer());
        setGenericSerializer(new GenericSerializer());

        try {
            final Class<?> clazz = Class.forName("stackunderflow.endersync.serializers.ItemStackSerializer_"+getVersion());
            if (ItemStackSerializer.class.isAssignableFrom(clazz)) {
                setItemStackSerializer((ItemStackSerializer) clazz.getConstructor().newInstance());
            }
        } catch (final Exception e) {
            e.printStackTrace();
            return;
        }

    }


    /*
     * Returns whether the current version is supported.
     */
    public boolean isCompatibleVersion() {
        if (getVersion().equals("v1_8_R1")) return true;
        if (getVersion().equals("v1_8_R2")) return true;
        if (getVersion().equals("v1_8_R3")) return true;
        if (getVersion().equals("v1_9_R1")) return true;
        if (getVersion().equals("v1_9_R2")) return true;
        if (getVersion().equals("v1_10_R1")) return true;
        if (getVersion().equals("v1_11_R1")) return true;
        if (getVersion().equals("v1_12_R1")) return true;
        if (getVersion().equals("v1_12_R2")) return true;
        if (getVersion().equals("v1_13_R1")) return true;
        if (getVersion().equals("v1_13_R2")) return true;
        return false;
    }


}
