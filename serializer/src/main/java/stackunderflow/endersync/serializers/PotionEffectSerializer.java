package stackunderflow.endersync.serializers;


import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.Collection;

/*
 * Serialize PotionEffects.
 */
public class PotionEffectSerializer {

    /*
     * PotionEffect
     */
    public String SerializePotionEffect(PotionEffect potionEffect) {

        String serialized = "t@";
        serialized += potionEffect.getType().getName();
        serialized += ";a@" + Integer.toString(potionEffect.getAmplifier());
        serialized += ";d@" + Integer.toString(potionEffect.getDuration());

        return serialized;
    }
    public PotionEffect DeserializePotionEffect(String serializedString) {

        String[] parts = serializedString.split(";");
        PotionEffectType type = PotionEffectType.SLOW;
        int amplifier = 0;
        int duration = 0;

        for (String part : parts) {
            String[] data = part.split("@");
            if (data[0].equals("t")) {
                try {
                    type = PotionEffectType.getByName(data[1]);
                }
                catch (Exception e) {
                    System.out.println("PotionType: "+data[1]);
                    e.printStackTrace();
                    return null;
                }
            }
            if (data[0].equals("d")) duration = Integer.parseInt(data[1]);
            if (data[0].equals("a")) amplifier = Integer.parseInt(data[1]);
        }

        PotionEffect potionEffect = new PotionEffect(type, duration, amplifier);

        return potionEffect;
    }
    public String SerializePotionEffects(Collection<PotionEffect> potionEffects) {

        String serialized = "";
        for (PotionEffect potionEffect : potionEffects) {
            serialized += SerializePotionEffect(potionEffect) + "&";
        }
        if (serialized.endsWith("&")) serialized = serialized.substring(0, serialized.length()-1);

        return serialized;
    }
    public ArrayList<PotionEffect> DeserializePotionEffects(String serializedString) {

        if (serializedString.equals("")) return new ArrayList<PotionEffect>();

        ArrayList<PotionEffect> potionEffects = new ArrayList<PotionEffect>();
        for (String serializedPotionEffect : serializedString.split("&")) {

            PotionEffect potionEffect = DeserializePotionEffect(serializedPotionEffect);
            if (potionEffect == null) return null;
            potionEffects.add(potionEffect);

        }

        return potionEffects;

    }

}
