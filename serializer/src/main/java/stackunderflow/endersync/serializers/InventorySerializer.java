package stackunderflow.endersync.serializers;

import org.bukkit.Bukkit;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import java.util.ArrayList;
import java.util.List;

/*
 * Serialize inventories.
 */
public class InventorySerializer {


    /*
     * Serialize inventory.
     */
    public String inventoryToBase64(Inventory inventory) {

        String serialized = inventory.getSize() + ";";
        int i = 0;
        for (org.bukkit.inventory.ItemStack itemStack : inventory.getContents()) {
            if (itemStack != null) {
                serialized = serialized + Integer.toString(i) + "@" + Serializer.INSTANCE.getItemStackSerializer().toBase64(itemStack) + "&";
            }
            i++;
        }
        if (serialized.endsWith("&")) serialized = serialized.substring(0, serialized.length()-1);
        return serialized;

    }


    /*
     * Deserialize inventory.
     */
    public Inventory inventoryFromBase64(String data) {
        return inventoryFromBase64(data, "null");
    }
    public Inventory inventoryFromBase64(String data, String title, InventoryType type) {

        // Process meta like "size".
        if (data.contains(";")) {
            String[] meta = data.split(";");
            if (meta.length >= 2) data = meta[1];
        }

        // Create inventory.
        Inventory inventory = Bukkit.getServer().createInventory(null, type, title);

        // Process items
        if (data.contains("&")) {
            for (String part : data.split("&")) {
                Integer position = Integer.parseInt(part.split("@")[0]);
                ItemStack item = Serializer.INSTANCE.getItemStackSerializer().fromBase64(part.split("@")[1]);
                inventory.setItem(position, item);
            }
        }

        return inventory;

    }
    public Inventory inventoryFromBase64(String data, String title) {

        // Process meta like "size".
        int size = 9;
        if (data.contains(";")) {
            String[] meta = data.split(";");
            if (meta.length != 0) {
                size = Integer.parseInt(meta[0]);
            }
            if (meta.length >= 2) data = meta[1];
        }

        System.out.println("Inv serialize:");
        System.out.println("Size: "+size);

        // Create inventory.
        Inventory inventory = Bukkit.getServer().createInventory(null, size, title);

        // Process items
        if (data.contains("&")) {
            for (String part : data.split("&")) {
                Integer position = Integer.parseInt(part.split("@")[0]);
                ItemStack item = Serializer.INSTANCE.getItemStackSerializer().fromBase64(part.split("@")[1]);
                inventory.setItem(position, item);
            }
        }

        return inventory;

    }

}
