package stackunderflow.endersync.serializers;


/*
 * Some generic serialization.
 */
public class GenericSerializer {

    /*
     * boolean
     */
    public int SerializeBoolean(boolean bool) {
        return bool ? 1 : 0;
    }
    public boolean DeserializeBoolean(Object bool) {
        if (bool instanceof Integer) {
            return (Integer) bool == 1;
        }
        else if (bool instanceof Boolean) {
            return (Boolean) bool;
        }
        return false;
    }

}
