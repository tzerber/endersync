package stackunderflow.endersync.database.mysql;

import lombok.Getter;
import lombok.Setter;


/*
 * Represents a column in a table.
 */
@Getter
@Setter
public class Column {

    // ================================     VARS

    // References
    // FOO


    // Settings
    private String name;
    private String type;




    // ================================     CONSTRUCTOR

    public Column(String name, String type) {
        setName(name);
        setType(type);
    }



    // ================================     LOGIC

}
