package stackunderflow.endersync.database.mysql;


import com.mysql.jdbc.Connection;
import lombok.Getter;
import lombok.Setter;
import stackunderflow.endersync.Main;
import stackunderflow.endersync.utils.Configuration;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/*
 * Contains basic database management functionality.
 */
@Getter
@Setter
public class DatabaseManager {

    // ================================     VARS

    // References
    public static DatabaseManager INSTANCE;
    public Connection connection;



    // ================================     CONSTRUCTOR

    public DatabaseManager() {
        if (DatabaseManager.INSTANCE == null) {
            DatabaseManager.INSTANCE = this;
        }
    }



    // ================================     LOGIC

    /*
     * Opens a connection to the database if none is present.
     */
    public void openConnection(String host, String port, String database, String user, String password) throws SQLException {

        // RET: Connection already open.
        if (getConnection() != null && !getConnection().isClosed()) return;

        // Open connection.
        setConnection(
                (Connection) DriverManager.getConnection("jdbc:mysql://"+host+":"+port+"/"+database, user, password)
        );

    }
    public boolean openConnection() {
        try {
            openConnection(
                    Configuration.INSTANCE.get().getString("database.host", "127.0.0.1"),
                    Integer.toString(Configuration.INSTANCE.get().getInt("database.port", 3306)),
                    Configuration.INSTANCE.get().getString("database.databaseName", "endersync"),
                    Configuration.INSTANCE.get().getString("database.user", "enderSyncUser"),
                    Configuration.INSTANCE.get().getString("database.password", "enderSync!123")
            );
            return true;
        } catch (SQLException e) {
            Main.INSTANCE.logError("Could not re-establish database connection after it was dropped!");
            Main.INSTANCE.logError("Please check your config.yml and restart the server!");
            return false;
        }
    }


    /*
     * Closes the connection to the database if one is present.
     */
    public void closeConnection() throws SQLException {
        if (getConnection() != null) getConnection().close();
    }


    /*
     * Return whether the connection is open.
     */
    public boolean isConnected() {
        if (getConnection() == null) return false;
        try {
            return !getConnection().isClosed();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }


    /*
     * Creates a table if it does not exist.
     */
    public void createTable(String name, String columns) throws SQLException {

        String sql = "CREATE TABLE IF NOT EXISTS "+name+" ("+columns+");";
        this.executeUpdate(sql);

    }


    /*
     * Creates multiple tables if they don't exist.
     */
    public void createTables(String[] names, String[] columns) throws SQLException {
        int i = 0;
        for (String name : names) {
            this.createTable(name, columns[i]);
            i++;
        }
    }



    // ================================     EXECUTION


    /*
     * Executes an update statement.
     */
    public void executeUpdate(String sql) throws SQLException {

        // RET: No connection.
        if (!this.isConnected() && !openConnection()) return;

        // Execute the statement.
        Statement statement = getConnection().createStatement();

        Main.INSTANCE.logDebug("Executing SQL: " + sql);

        statement.executeUpdate(sql);
        statement.close();

    }

    /*
     * Executes statement and returns data.
     * !! Remember to manually close the statement !!
     */
    public ResultSet executeQuery(String sql) throws SQLException {

        // RET: No connection.
        if (!this.isConnected() && !openConnection()) return null;

        Main.INSTANCE.logDebug("Executing SQL: " + sql);

        // Execute the statement.
        Statement statement = getConnection().createStatement();
        return statement.executeQuery(sql);

    }

}
