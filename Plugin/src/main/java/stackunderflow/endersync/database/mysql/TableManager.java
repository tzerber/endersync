package stackunderflow.endersync.database.mysql;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.OfflinePlayer;
import stackunderflow.endersync.Main;
import stackunderflow.endersync.exceptions.RowNotFoundException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


/*
 * Represents a manages table in the MySQL database
 */
@Getter
@Setter
public class TableManager {

    // ================================     VARS

    // References
    private ArrayList<Column> columns;


    // Settings
    private String tableName;




    // ================================     CONSTRUCTOR

    public TableManager(String tableName) {
        setColumns(new ArrayList<>());
        setTableName(tableName);

        // Add id column.
        addColumn("id", "VARCHAR(36) NOT NULL UNIQUE, INDEX (id)");
    }



    // ================================     LOGIC


    /*
     * Adds a column to the table with a simple data type.
     * -> For basic users.
     */
    public void addColumn(String name, SimpleColumnType type) {

        // Convert type.
        String dataType = "";
        switch (type) {
            case INT: dataType="INT"; break;
            case STRING: dataType="TEXT"; break;
            case BOOLEAN: dataType="BOOLEAN"; break;
        }

        // Add the column.
        addColumn(name, dataType);

    }


    /*
     * Adds a column to the table with a named MySQL data type.
     * -> For advanced users.
     */
    public void addColumn(String name, String type) {
        getColumns().add(new Column(name, type));
    }


    /*
     * Return various SQL strings.
     */
    public String getColumnsSQLString() {

        // Concat the string & Removes the last ",".
        String sqlString = "";
        for (Column col : getColumns()) {
            sqlString += col.getName()+" "+col.getType() + ",";
        }
        sqlString = sqlString.substring(0, sqlString.length() - 1);

        return sqlString;

    }
    public String[] getColumnNameValuesSQLString(Row row) {

        // Concat the strings & Removes the last ",".
        String sqlNameString = "";
        String sqlValueString = "";
        String sqlUpdateString = "";
        for (String columnName : row.getValues().keySet()) {
            sqlNameString = sqlNameString + columnName+",";
            sqlValueString = sqlValueString + "'"+row.get(columnName)+"'"+",";
            sqlUpdateString = sqlUpdateString + columnName+"="+"'"+row.get(columnName)+"',";
        }
        sqlNameString = sqlNameString.substring(0, sqlNameString.length() - 1);
        sqlValueString = sqlValueString.substring(0, sqlValueString.length() - 1);
        sqlUpdateString = sqlUpdateString.substring(0, sqlUpdateString.length() - 1);

        return new String[]{ sqlNameString, sqlValueString, sqlUpdateString };

    }


    /*
     * Ensures that the table exists by creating it if it does not.
     */
    public void createTable() throws SQLException {
        DatabaseManager.INSTANCE.createTable(getTableName(), getColumnsSQLString());
    }


    /*
     * Gets a row from the table by a simple single columng single value query.
     */
    public Row getRow(String queryColumnName, String queryColumnValue) throws SQLException, RowNotFoundException {

        String sql = "SELECT * FROM "+getTableName()+" WHERE "+queryColumnName+"='"+queryColumnValue+"' limit 1";
        return getRow(sql);

    }


    /*
     * Gets a row from the table by a SQL query.
     */
    public Row getRow(String querySQLString) throws SQLException, RowNotFoundException {

        ResultSet result = DatabaseManager.INSTANCE.executeQuery(querySQLString);

        // RET: Probably no connection!
        if (result == null) throw new RowNotFoundException("Could not find row by using '"+querySQLString+"'");

        // Populate row object with data.
        Row row = new Row(this);
        try {

            // Only get values if it actually returned a row.
            if (result.first()) {
                for (int i = 1; i <= result.getMetaData().getColumnCount(); i++) {
                    row.set(result.getMetaData().getColumnLabel(i), result.getObject(i));
                }
            }

        }
        catch (SQLException e) {
            Main.INSTANCE.logError("Could not populate rowWrapper instance!");
            e.printStackTrace();
        }

        return row;

    }


    public List<Row> getRows(String querySQLString) throws SQLException, RowNotFoundException {

        ResultSet result = DatabaseManager.INSTANCE.executeQuery(querySQLString);

        // RET: Probably no connection!
        if (result == null) throw new RowNotFoundException("Could not find rows by using '"+querySQLString+"'");

        // Populate row object with data.
        List<Row> rows = new ArrayList<>();
        try {

            // Only get values if it actually returned a row.
            if (result.first()) {
                Row row = new Row(this);
                for (int i = 1; i <= result.getMetaData().getColumnCount(); i++) {
                    row.set(result.getMetaData().getColumnLabel(i), result.getObject(i));
                }
                rows.add(row);
            }

        }
        catch (SQLException e) {
            Main.INSTANCE.logError("Could not populate rowWrapper instance(s)!");
            e.printStackTrace();
        }

        return rows;

    }


    /*
     * A wrapper to quickly get a player row.
     * If it does not exist the newly created one will automatically
     * be populated with player data like uuid and name.
     */
    public Row getPlayerRow(OfflinePlayer player) throws SQLException, RowNotFoundException {

        return getPlayerRow(player.getUniqueId());

        /*Row row = getRow("player_uuid", player.getUniqueId().toString());

        // RET: No row found.
        if (row == null) return null;

        row.set("player_uuid", player.getUniqueId().toString());
        //row.set("player_name", player.getName());
        return row;*/

    }
    public Row getPlayerRow(UUID playerUUID) throws SQLException, RowNotFoundException {

        Row row = getRow("player_uuid", playerUUID.toString());

        // RET: No row found.
        if (row == null) return null;

        row.set("player_uuid", playerUUID.toString());
        return row;

    }


    /*
     * Updates / Inserts a row depending on whether it exists.
     */
    public boolean insertOrUpdateRow(Row row) {

        String[] sqlStrings = getColumnNameValuesSQLString(row);
        String sql = "INSERT INTO "+getTableName()+" ("+sqlStrings[0]+") VALUES ("+sqlStrings[1]+") ON DUPLICATE KEY UPDATE "+sqlStrings[2];
        try {
            DatabaseManager.INSTANCE.executeUpdate(sql);
        } catch (SQLException e) {
            Main.INSTANCE.logError("Could not insertOrUpdate row!");
            e.printStackTrace();
            return false;
        }

        return true;

    }


    /*
     * Deletes a row.
     */
    public boolean deleteRow(Row row) {

        String sql = "DELETE FROM "+getTableName()+" WHERE id='"+row.get("id")+"'";
        try {
            DatabaseManager.INSTANCE.executeUpdate(sql);
        } catch (SQLException e) {
            Main.INSTANCE.logError("Could not delete row!");
            e.printStackTrace();
            return false;
        }
        return true;

    }


}
