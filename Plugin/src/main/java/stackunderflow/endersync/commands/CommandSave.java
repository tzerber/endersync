package stackunderflow.endersync.commands;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import stackunderflow.endersync.SyncManager;
import stackunderflow.endersync.utils.Configuration;
import stackunderflow.endersync.utils.Promise;
import stackunderflow.endersync.utils.StringFormatter;
import stackunderflow.libs.commandfactory.Command;
import stackunderflow.libs.commandfactory.CommandArg;

import java.util.Map;


/*
 * The skin command which can be used to change other players or one own's skin.
 */
@Getter
@Setter
public class CommandSave extends Command {

    // ================================     VARS

    // Null



    // ================================     CONSTRUCTOR


    public CommandSave(String name) {
        super(name);

        // Setup tokens.
        this
                .addToken("save")
                .addToken("[name]");

    }



    // ================================     COMMAND LOGIC


    @Override
    public void onPlayerCall(Player sender, Map<String, CommandArg> args) {

        // Save other.
        if (args.get("[name]").isGiven()) {

            // RET: No permission
            if (!sender.hasPermission("endersync.admin.save.other")) {
                if (Configuration.INSTANCE.get().getBoolean("plugin.displayErrorMessagesToPlayers")) {
                    sender.sendMessage(new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("general.noPermission"))
                            .getSTR()
                    );
                }
                return;
            }

            Player target = Bukkit.getServer().getPlayer(args.get("[name]").getValue());

            // ERR: Player not found.
            if (target == null) {
                sender.sendMessage(
                        new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("general.playerNotFound")).getSTR()
                );
                return;
            }

            // Start save.
            SyncManager.INSTANCE.savePlayer(target, new Promise() {
                @Override
                public void onSuccess() {}
                @Override
                public void onError(Exception ex) {}
            });

            // Info.
            sender.sendMessage(
                    new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("command.cmdInfoSaveStarted"))
                            .replace("{player}", target.getName())
                            .getSTR()
            );

        }

        // Save self.
        else {

            // RET: No permission
            if (!sender.hasPermission("endersync.admin.save.self")) {
                if (Configuration.INSTANCE.get().getBoolean("plugin.displayErrorMessagesToPlayers")) {
                    sender.sendMessage(new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("general.noPermission"))
                            .getSTR()
                    );
                }
                return;
            }

            // Start save.
            SyncManager.INSTANCE.savePlayer(sender, new Promise() {
                @Override
                public void onSuccess() {}
                @Override
                public void onError(Exception ex) {}
            });

        }

    }


    @Override
    public void onConsoleCall(CommandSender sender, Map<String, CommandArg> args) {

        // Save other.
        if (args.get("[name]").isGiven()) {

            Player target = Bukkit.getServer().getPlayer(args.get("[name]").getValue());

            // ERR: Player not found.
            if (target == null) {
                sender.sendMessage(
                        new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("general.playerNotFound")).getSTR()
                );
                return;
            }

            // Start save.
            SyncManager.INSTANCE.savePlayer(target, new Promise() {
                @Override
                public void onSuccess() {}
                @Override
                public void onError(Exception ex) {}
            });

            // Info.
            sender.sendMessage(
                    new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("command.cmdInfoSaveStarted"))
                            .replace("{player}", target.getName())
                            .getSTR()
            );

        }

        // ERR: Cannot save console!
        else {
            sender.sendMessage(
                    new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("general.consoleCannotExecute")).getSTR()
            );
        }

    }

}
