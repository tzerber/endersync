package stackunderflow.endersync.commands.administration;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.entity.Player;
import stackunderflow.endersync.SyncManager;
import stackunderflow.endersync.modules.PlayerSyncModule;
import stackunderflow.endersync.utils.*;
import stackunderflow.libs.commandfactory.Command;
import stackunderflow.libs.commandfactory.CommandArg;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;


/*
 * The skin command which can be used to change other players or one own's skin.
 */
@Getter
@Setter
public class CommandReload extends Command {

    // ================================     VARS

    // Null



    // ================================     CONSTRUCTOR


    public CommandReload(String name) {
        super(name);

        // Setup tokens.
        this
                .addToken("endersync", "es")
                .addToken("reload");

    }



    // ================================     COMMAND LOGIC


    @Override
    public void onPlayerCall(Player sender, Map<String, CommandArg> args) {

        // RET: No permission
        if (!BukkitUtils.playerHasPermission(sender, "endersync.admin.reload")) {
            if (Configuration.INSTANCE.get().getBoolean("plugin.displayErrorMessagesToPlayers")) {
                sender.sendMessage(new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("general.noPermission"))
                        .getSTR()
                );
            }
            return;
        }

        // Reload config.
        try {
            Configuration.INSTANCE.reloadConfig();
            Configuration.INSTANCE.reloadConfig("features");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }

        // Create all database tables. Need this for hotswitching modules.
        for (PlayerSyncModule module : SyncManager.INSTANCE.getActivePlayerModules().values()) {
            try {
                module.getTableManager().createTable();
            } catch (SQLException e) {
                // TODO: Add err msg
                e.printStackTrace();
            }
        }

        // Info.
        sender.sendMessage(new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("general.configReloadCompleted"))
                .setSuccess(true)
                .getSTR()
        );

    }


    @Override
    public void onConsoleCall(CommandSender sender, Map<String, CommandArg> args) {

        // Reload config.
        try {
            Configuration.INSTANCE.reloadConfig();
            Configuration.INSTANCE.reloadConfig("features");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }

        // Create all database tables. Need this for hotswitching modules.
        for (PlayerSyncModule module : SyncManager.INSTANCE.getActivePlayerModules().values()) {
            try {
                module.getTableManager().createTable();
            } catch (SQLException e) {
                // TODO: Add err msg
                e.printStackTrace();
            }
        }

        // Info.
        sender.sendMessage(new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("general.configReloadCompleted"))
                .setSuccess(true)
                .getSTR()
        );

    }

}
