package stackunderflow.endersync.commands;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import stackunderflow.endersync.SyncManager;
import stackunderflow.endersync.utils.Configuration;
import stackunderflow.endersync.utils.Promise;
import stackunderflow.endersync.utils.StringFormatter;
import stackunderflow.libs.commandfactory.Command;
import stackunderflow.libs.commandfactory.CommandArg;

import java.util.Map;


/*
 * The skin command which can be used to change other players or one own's skin.
 */
@Getter
@Setter
public class CommandSync extends Command {

    // ================================     VARS

    // Null



    // ================================     CONSTRUCTOR


    public CommandSync(String name) {
        super(name);

        // Setup tokens.
        this
                .addToken("sync")
                .addToken("[name]");

    }



    // ================================     COMMAND LOGIC


    @Override
    public void onPlayerCall(Player sender, Map<String, CommandArg> args) {

        // Sync other.
        if (args.get("[name]").isGiven()) {

            // RET: No permission
            if (!sender.hasPermission("endersync.admin.sync.other")) {
                if (Configuration.INSTANCE.get().getBoolean("plugin.displayErrorMessagesToPlayers")) {
                    sender.sendMessage(new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("general.noPermission"))
                            .getSTR()
                    );
                }
                return;
            }

            Player target = Bukkit.getServer().getPlayer(args.get("[name]").getValue());

            // ERR: Player not found.
            if (target == null) {
                sender.sendMessage(
                        new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("general.playerNotFound")).getSTR()
                );
                return;
            }

            // Start sync.
            SyncManager.INSTANCE.syncPlayer(target, new Promise() {
                @Override
                public void onSuccess() {}
                @Override
                public void onError(Exception ex) {}
            });

            // Info.
            sender.sendMessage(
                    new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("command.cmdInfoSyncStarted"))
                            .replace("{player}", target.getName())
                            .getSTR()
            );

        }

        // Sync self.
        else {

            // RET: No permission
            if (!sender.hasPermission("endersync.admin.sync.self")) {
                if (Configuration.INSTANCE.get().getBoolean("plugin.displayErrorMessagesToPlayers")) {
                    sender.sendMessage(new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("general.noPermission"))
                            .getSTR()
                    );
                }
                return;
            }

            // Start sync.
            SyncManager.INSTANCE.syncPlayer(sender, new Promise() {
                @Override
                public void onSuccess() {}
                @Override
                public void onError(Exception ex) {}
            });

        }

    }


    @Override
    public void onConsoleCall(CommandSender sender, Map<String, CommandArg> args) {

        // Sync other.
        if (args.get("[name]").isGiven()) {

            Player target = Bukkit.getServer().getPlayer(args.get("[name]").getValue());

            // ERR: Player not found.
            if (target == null) {
                sender.sendMessage(
                        new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("general.playerNotFound")).getSTR()
                );
                return;
            }

            // Start sync.
            SyncManager.INSTANCE.syncPlayer(target, new Promise() {
                @Override
                public void onSuccess() {}
                @Override
                public void onError(Exception ex) {}
            });

            // Info.
            sender.sendMessage(
                    new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("command.cmdInfoSyncStarted"))
                            .replace("{player}", target.getName())
                            .getSTR()
            );

        }

        // ERR: Cannot sync console!
        else {
            sender.sendMessage(
                    new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("general.consoleCannotExecute")).getSTR()
            );
        }

    }

}
