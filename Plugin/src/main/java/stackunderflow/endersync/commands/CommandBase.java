package stackunderflow.endersync.commands;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import stackunderflow.endersync.Main;
import stackunderflow.libs.commandfactory.Command;
import stackunderflow.libs.commandfactory.CommandArg;

import java.util.Map;


/*
 * The skin command which can be used to change other players or one own's skin.
 */
@Getter
@Setter
public class CommandBase extends Command {

    // ================================     VARS

    // Null



    // ================================     CONSTRUCTOR


    public CommandBase(String name) {
        super(name);

        // Setup tokens.
        this.addToken("endersync", "es");

    }



    // ================================     COMMAND LOGIC


    @Override
    public void onPlayerCall(Player sender, Map<String, CommandArg> args) {
        sender.sendMessage("");
        sender.sendMessage("§7§l-=-=-=-=-=-< §3§lEnder§l§fSync§7§l >-=-=-=-=-=-=-");
        sender.sendMessage("");
        sender.sendMessage("   §7Version: §ev"+Main.INSTANCE.getVersion());
        sender.sendMessage("   §7Author: §eStackUnderflow(_)");
        sender.sendMessage("   §7Support: §ehttps://discord.gg/YFArBG7");
        sender.sendMessage("   §7Server: §ewoodpixel.net");
        sender.sendMessage("");
        sender.sendMessage("   §7Use §b/es help §7for more info.");
        sender.sendMessage("");
        sender.sendMessage("§7§l-=-=-=-=-=-< §7§l--------- >-=-=-=-=-=-=-");
        sender.sendMessage("");
    }


    @Override
    public void onConsoleCall(CommandSender sender, Map<String, CommandArg> args) {
        sender.sendMessage("");
        sender.sendMessage("§7§l-=-=-=-=-=-< §3§lEnder§l§fSync§7§l >-=-=-=-=-=-=-");
        sender.sendMessage("");
        sender.sendMessage("   §7Version: §ev"+Main.INSTANCE.getVersion());
        sender.sendMessage("   §7Author: §eStackUnderflow(_)");
        sender.sendMessage("   §7Support: §ehttps://discord.gg/YFArBG7");
        sender.sendMessage("   §7Server: §ewoodpixel.net");
        sender.sendMessage("");
        sender.sendMessage("   §7Use §b/es help §7for more info.");
        sender.sendMessage("");
        sender.sendMessage("§7§l-=-=-=-=-=-< §7§l--------- >-=-=-=-=-=-=-");
        sender.sendMessage("");
    }

}
