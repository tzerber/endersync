package stackunderflow.endersync.commands;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import stackunderflow.endersync.utils.BukkitUtils;
import stackunderflow.endersync.utils.Configuration;
import stackunderflow.endersync.utils.StringFormatter;
import stackunderflow.libs.commandfactory.Command;
import stackunderflow.libs.commandfactory.CommandArg;

import java.util.Map;


/*
 * The skin command which can be used to change other players or one own's skin.
 */
@Getter
@Setter
public class CommandHelp extends Command {

    // ================================     VARS

    // Null



    // ================================     CONSTRUCTOR


    public CommandHelp(String name) {
        super(name);

        // Setup tokens.
        this
                .addToken("endersync", "es")
                .addToken("help");

    }



    // ================================     COMMAND LOGIC


    @Override
    public void onPlayerCall(Player sender, Map<String, CommandArg> args) {

        // RET: No permission
        if (!BukkitUtils.playerHasPermission(sender, "endersync.admin.help")) {
            if (Configuration.INSTANCE.get().getBoolean("plugin.displayErrorMessagesToPlayers")) {
                sender.sendMessage(new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("general.noPermission"))
                        .getSTR()
                );
            }
            return;
        }

        for (String string : Configuration.INSTANCE.get("lang").getStringList("messages.helpText")) {
            sender.sendMessage(new StringFormatter(string)
                    .getSTR()
            );
        }
    }


    @Override
    public void onConsoleCall(CommandSender sender, Map<String, CommandArg> args) {
        for (String string : Configuration.INSTANCE.get("lang").getStringList("messages.helpText")) {
            sender.sendMessage(new StringFormatter(string)
                    .getSTR()
            );
        }
    }

}
