package stackunderflow.endersync.commands.manipulation;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import stackunderflow.endersync.Main;
import stackunderflow.endersync.SyncManager;
import stackunderflow.endersync.database.mysql.Row;
import stackunderflow.endersync.exceptions.ModuleExceptions;
import stackunderflow.endersync.modules.PlayerSyncModule;
import stackunderflow.endersync.utils.*;
import stackunderflow.libs.commandfactory.Command;
import stackunderflow.libs.commandfactory.CommandArg;

import java.sql.SQLException;
import java.util.Map;
import java.util.UUID;


/*
 * The player can set other players vault balances.
 */
@Getter
@Setter
public class CommandEcoSet extends Command {

    // ================================     VARS

    // Null



    // ================================     CONSTRUCTOR


    public CommandEcoSet(String name) {
        super(name);

        // Setup tokens.
        this
                .addToken("endersync", "es")
                .addToken("ecoSet")
                .addToken("<name>")
                .addToken("<amount>");

    }



    // ================================     COMMAND LOGIC


    @Override
    public void onPlayerCall(Player sender, Map<String, CommandArg> args) {

        // RET: No permission
        if (!BukkitUtils.playerHasPermission(sender, "endersync.admin.ecoSet")) {
            if (Configuration.INSTANCE.get().getBoolean("plugin.displayErrorMessagesToPlayers")) {
                sender.sendMessage(new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("general.noPermission"))
                        .getSTR()
                );
            }
            return;
        }

        // Update.
        updatePlayerBalance(sender, args.get("<name>").getValue(), args.get("<amount>").getValue());

    }


    @Override
    public void onConsoleCall(CommandSender sender, Map<String, CommandArg> args) {

        // Update.
        updatePlayerBalance(sender, args.get("<name>").getValue(), args.get("<amount>").getValue());

    }



    private void updatePlayerBalance(CommandSender sender, String target, String amount) {

        // Save the target player if he is online.
        if (Bukkit.getPlayer(target) != null) {
            SyncManager.INSTANCE.savePlayer(Bukkit.getPlayer(target), new Promise() {
                @Override
                public void onSuccess() {}
                @Override
                public void onError(Exception ex) {}
            });
        }

        /*try {

            final double amountValue = Double.parseDouble(amount);
            final PlayerSyncModule vaultModule = (PlayerSyncModule) SyncManager.INSTANCE.getModule("vault_economy");

            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.INSTANCE, () -> {

                // Get the inventory row.
                Row row;
                try {

                    UUID targetUuid = UUIDUtil.getUUID(target);
                    if (targetUuid == null) {
                        new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("general.playerNotFound"))
                                .replace("{player}", target).sendMessageTo(sender);
                        return;
                    }

                    row = Main.API.getPlayerDataRow(Bukkit.getOfflinePlayer(targetUuid), vaultModule);
                } catch (SQLException | DatabaseExceptions.RowNotFoundException e) {
                    sender.sendMessage(new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("general.generalError"))
                            .getSTR())
                    ;
                    e.printStackTrace();
                    return;
                }

                row.set("balance_value", amountValue);
                row.update();

                // Sync the target player if he is online.
                if (Bukkit.getPlayer(target) != null) {
                    SyncManager.INSTANCE.syncPlayer(Bukkit.getPlayer(target), new Promise() {
                        @Override
                        public void onSuccess() {}
                        @Override
                        public void onError(Exception ex) {}
                    });
                }

                sender.sendMessage(new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("command.cmdEcoSetSuccess"))
                        .replace("{balance}", Double.toString((double) row.get("balance_value")))
                        .replace("{player}", target)
                        .setSuccess(true)
                        .getSTR()
                );

            }, 20L);

        }
        catch (ModuleExceptions.ModuleNotFoundException e) {
            // TODO: Add module not enabled player message..
            return;
        }
        catch (NumberFormatException e) {
            sender.sendMessage(new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("command.cmdEcoSetFailed"))
                    .setSuccess(false)
                    .getSTR()
            );
            return;
        }*/

    }

}
