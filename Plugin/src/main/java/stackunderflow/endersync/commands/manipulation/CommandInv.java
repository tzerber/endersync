package stackunderflow.endersync.commands.manipulation;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import stackunderflow.endersync.Main;
import stackunderflow.endersync.SyncManager;
import stackunderflow.endersync.database.mysql.Row;
import stackunderflow.endersync.exceptions.ModuleNotFoundException;
import stackunderflow.endersync.exceptions.RowNotFoundException;
import stackunderflow.endersync.modules.PlayerSyncModule;
import stackunderflow.endersync.utils.*;
import stackunderflow.endersync.serializers.Serializer;
import stackunderflow.libs.commandfactory.Command;
import stackunderflow.libs.commandfactory.CommandArg;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


/*
 * The skin command which can be used to change other players or one own's skin.
 */
@Getter
@Setter
public class CommandInv extends Command {

    // ================================     VARS

    public static HashMap<String, String> activeInvseePlayer;



    // ================================     CONSTRUCTOR


    public CommandInv(String name) {
        super(name);

        activeInvseePlayer = new HashMap<>();

        // Setup tokens.
        this
                .addToken("endersync", "es")
                .addToken("inv")
                .addToken("<name>");

    }



    // ================================     COMMAND LOGIC


    @Override
    public void onPlayerCall(Player sender, Map<String, CommandArg> args) {

        // RET: No permission
        if (!BukkitUtils.playerHasPermission(sender, "endersync.admin.inv")) {
            if (Configuration.INSTANCE.get().getBoolean("plugin.displayErrorMessagesToPlayers")) {
                sender.sendMessage(new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("general.noPermission"))
                        .getSTR()
                );
            }
            return;
        }


        try {

            final PlayerSyncModule inventoryModule = (PlayerSyncModule) SyncManager.INSTANCE.getModule("inventory");

            // Save the target player to make inventory live.
            if (Bukkit.getPlayer(args.get("<name>").getValue()) != null) {
                SyncManager.INSTANCE.savePlayer(Bukkit.getPlayer(args.get("<name>").getValue()), inventoryModule, new Promise() {
                    @Override
                    public void onSuccess() {}
                    @Override
                    public void onError(Exception ex) {}
                });
            }

            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.INSTANCE, () -> {

                // Get the inventory row.
                Row row;
                try {
                    UUID targetUuid = UUIDUtil.getUUID(args.get("<name>").getValue());
                    if (targetUuid == null) {
                        new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("command.cmdInvNoPlayerDataFound"))
                                .replace("{player}", args.get("<name>").getValue()).sendMessageTo(sender);
                        return;
                    }
                    row = Main.API.getPlayerDataRow(Bukkit.getOfflinePlayer(targetUuid), inventoryModule);
                } catch (SQLException | RowNotFoundException e) {
                    e.printStackTrace();
                    return;
                }

                // RET: No player data found.
                if (row.isEmpty() || row.get("inventory_encoded") == null) {
                    new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("command.cmdInvNoPlayerDataFound"))
                            .replace("{player}", args.get("<name>").getValue()).sendMessageTo(sender);
                    return;
                }

                Inventory playerInventory = Serializer.INSTANCE.getInventorySerializer().inventoryFromBase64((String) row.get("inventory_encoded"), "§bInv of §e"+args.get("<name>").getValue(), InventoryType.PLAYER);
                activeInvseePlayer.put(sender.getName(), args.get("<name>").getValue());
                sender.openInventory(playerInventory);

            }, 20L);

        }
        catch (ModuleNotFoundException e) {
            // TODO: Add module not enabled player message..
            return;
        }

    }


    @Override
    public void onConsoleCall(CommandSender sender, Map<String, CommandArg> args) {
        sender.sendMessage(
                new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("general.consoleCannotExecute")).getSTR()
        );
    }

}
