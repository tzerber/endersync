package stackunderflow.endersync.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import stackunderflow.endersync.Main;
import stackunderflow.endersync.SyncManager;
import stackunderflow.endersync.exceptions.ModuleNotFoundException;
import stackunderflow.endersync.modules.PlayerSyncModule;
import stackunderflow.endersync.utils.Configuration;
import stackunderflow.endersync.utils.StringFormatter;


/*
 * Handles player command calls.
 * -> Cancel them if blocked.
 */
public class CommandPreprocessListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onCommandPreprocess(PlayerCommandPreprocessEvent event) {

        Player player = event.getPlayer();

        if (Main.INSTANCE.vault_sync_enable) {
            PlayerSyncModule vaultModule = null;
            try {
                vaultModule = (PlayerSyncModule) SyncManager.INSTANCE.getModule("vault_economy");
            } catch (ModuleNotFoundException e) {
                e.printStackTrace();
                event.setCancelled(true);
                return;
            }

            // Cancel if player is blocked.
            if (vaultModule.isPlayerBlocked(player)) {
                event.setCancelled(true);
                player.sendMessage(
                        new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("sync.syncNoCommandsAllowed")).setSuccess(false).getSTR()
                );
            }
        }

    }

}
