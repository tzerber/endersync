package stackunderflow.endersync.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import stackunderflow.endersync.SyncManager;
import stackunderflow.endersync.exceptions.ModuleNotFoundException;
import stackunderflow.endersync.modules.PlayerSyncModule;


/*
 * Handles inventory clicks.
 * -> Cancel if player blocked.
 */
public class InventoryClickListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInventoryClick(InventoryClickEvent event) {

        Player player = (Player) event.getWhoClicked();

        if (SyncManager.INSTANCE.isModuleEnabled("inventory")) {
            PlayerSyncModule inventoryModule = null;
            try {
                inventoryModule = (PlayerSyncModule) SyncManager.INSTANCE.getModule("inventory");
            }
            catch (ModuleNotFoundException e) {
                event.setCancelled(true);
                return;
            }

            // Cancel if player is blocked.
            if (inventoryModule.isPlayerBlocked(player)) {
                event.setCancelled(true);
            }
        }
        if (SyncManager.INSTANCE.isModuleEnabled("enderchest")) {
            PlayerSyncModule enderChestModule = null;
            try {
                enderChestModule = (PlayerSyncModule) SyncManager.INSTANCE.getModule("enderchest");
            }
            catch (ModuleNotFoundException e) {
                event.setCancelled(true);
                return;
            }

            // Cancel if player is blocked.
            if (enderChestModule.isPlayerBlocked(player)) {
                event.setCancelled(true);
            }
        }

    }

}
