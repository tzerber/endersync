package stackunderflow.endersync.listeners;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import stackunderflow.endersync.Main;
import stackunderflow.endersync.SyncManager;
import stackunderflow.endersync.exceptions.ModuleNotFoundException;
import stackunderflow.endersync.modules.PlayerSyncModule;
import stackunderflow.endersync.utils.Messaging;
import stackunderflow.endersync.utils.Promise;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;


@Getter
@Setter
public class PluginMessageListener implements org.bukkit.plugin.messaging.PluginMessageListener {

    // ================================     VARS

    // References
    public static PluginMessageListener INSTANCE;

    // State
    private HashMap<UUID, Messaging.IOPCallback> occupiedPlayersCallbacks = new HashMap<>();



    // ================================     CONSTRUCTOR & SINGLETON

    public PluginMessageListener() {
        if (PluginMessageListener.INSTANCE == null) {
            PluginMessageListener.INSTANCE = this;
        }
    }



    // ================================     MESSAGING LOGIC

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {

        DataInputStream in = new DataInputStream(new ByteArrayInputStream(message));

        try {
            String subchannel = in.readUTF();


            // IOP (Is-Occupied Player)
            if (subchannel.equalsIgnoreCase("IOP")) {
                String data = in.readUTF();
                String[] splitData = data.split(";");
                UUID playerUUID = UUID.fromString(splitData[0]);
                String responseValue = splitData[1];

                if (getOccupiedPlayersCallbacks().containsKey(playerUUID)) {
                    Messaging.IOPCallback callback = getOccupiedPlayersCallbacks().get(playerUUID);
                    getOccupiedPlayersCallbacks().remove(playerUUID);
                    if (responseValue.equalsIgnoreCase("true")) callback.onTrue();
                    else callback.onFalse();
                }
            }


            // SYP
            if (subchannel.equalsIgnoreCase("SYP")) {

                // Prepare data.
                String data = in.readUTF();
                String[] splitData = data.split(";");
                UUID targetUUID = UUID.fromString(splitData[0]);
                String[] moduleNames = data.substring(splitData[0].length()+1, data.length()).split(";");

                Main.INSTANCE.logError(targetUUID.toString());
                System.out.println(moduleNames);

                // RET: Player not on server.
                Player targetPlayer = Bukkit.getPlayer(targetUUID);
                if (targetPlayer == null) return;

                // Get module list.
                List<PlayerSyncModule> modules = new ArrayList<>();
                for (String moduleName : moduleNames) {
                    try {
                        modules.add((PlayerSyncModule) SyncManager.INSTANCE.getModule(moduleName));
                    } catch (ModuleNotFoundException e) {
                        e.printStackTrace();
                    }
                }

                // Sync player.
                SyncManager.INSTANCE.syncPlayer(targetPlayer, modules, new Promise() {
                    @Override
                    public void onSuccess() {}
                    @Override
                    public void onError(Exception ex) {}
                });

            }


            // SAP
            if (subchannel.equalsIgnoreCase("SAP")) {

                // Prepare data.
                String data = in.readUTF();
                String[] splitData = data.split(";");
                UUID targetUUID = UUID.fromString(splitData[0]);
                String[] moduleNames = data.substring(splitData[0].length()+1, data.length()).split(";");

                Main.INSTANCE.logError(targetUUID.toString());
                System.out.println(moduleNames);

                // RET: Player not on server.
                Player targetPlayer = Bukkit.getPlayer(targetUUID);
                if (targetPlayer == null) return;

                // Get module list.
                List<PlayerSyncModule> modules = new ArrayList<>();
                for (String moduleName : moduleNames) {
                    try {
                        modules.add((PlayerSyncModule) SyncManager.INSTANCE.getModule(moduleName));
                    } catch (ModuleNotFoundException e) {
                        e.printStackTrace();
                    }
                }

                // Sync player.
                SyncManager.INSTANCE.savePlayer(targetPlayer, modules, new Promise() {
                    @Override
                    public void onSuccess() {}
                    @Override
                    public void onError(Exception ex) {}
                });

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
