package stackunderflow.endersync.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import stackunderflow.endersync.SyncManager;
import stackunderflow.endersync.exceptions.ModuleNotFoundException;
import stackunderflow.endersync.modules.PlayerSyncModule;


/*
 * Handles players dropping items.
 * -> Cancel them if blocked.
 */
public class DropItemListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onItemDrop(PlayerDropItemEvent event) {

        Player player = event.getPlayer();

        if (SyncManager.INSTANCE.isModuleEnabled("inventory")) {

            PlayerSyncModule inventoryModule = null;
            try {
                inventoryModule = (PlayerSyncModule) SyncManager.INSTANCE.getModule("inventory");
            }
            catch (ModuleNotFoundException e) {
                e.printStackTrace();
                event.setCancelled(true);
                return;
            }

            // Cancel if player is blocked.
            if (SyncManager.INSTANCE.isModuleEnabled("inventory") && inventoryModule.isPlayerBlocked(player)) {
                event.setCancelled(true);
            }

        }
        if (SyncManager.INSTANCE.isModuleEnabled("enderchest")){
            PlayerSyncModule enderChestModule = null;
            try {
                enderChestModule = (PlayerSyncModule) SyncManager.INSTANCE.getModule("enderchest");
            }
            catch (ModuleNotFoundException e) {
                e.printStackTrace();
                event.setCancelled(true);
                return;
            }

            // Cancel if player is blocked.
            if (SyncManager.INSTANCE.isModuleEnabled("enderchest") && enderChestModule.isPlayerBlocked(player)) {
                event.setCancelled(true);
            }
        }

    }

}
