package stackunderflow.endersync.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import stackunderflow.endersync.Main;
import stackunderflow.endersync.SyncManager;
import stackunderflow.endersync.commands.manipulation.CommandEnd;
import stackunderflow.endersync.commands.manipulation.CommandInv;
import stackunderflow.endersync.exceptions.ModuleNotFoundException;
import stackunderflow.endersync.exceptions.NonePlayersOnline;
import stackunderflow.endersync.modules.EnderChestPlayerSyncModule;
import stackunderflow.endersync.modules.PlayerSyncModule;
import stackunderflow.endersync.utils.Promise;

import java.util.UUID;


/*
 * Handles inventory closes.
 * -> Updates when using invsee.
 */
public class InventoryCloseListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInventoryClose(InventoryCloseEvent event) {

        Player player = (Player) event.getPlayer();
        Inventory inventory = event.getInventory();

        // Check if inventory is an enderchest, if so, save the inv.
        if (inventory.getType().equals(InventoryType.ENDER_CHEST) && SyncManager.INSTANCE.isModuleEnabled("enderchest") && !CommandEnd.activeInvseePlayer.keySet().contains(player.getName())) {

            // Get the module.
            PlayerSyncModule module = null;
            try {
                module = (PlayerSyncModule) SyncManager.INSTANCE.getModule("enderchest");
            }
            catch (ModuleNotFoundException e) {
                // TODO: Add module not enabled player message..
                return;
            }

            SyncManager.INSTANCE.savePlayer(player, module, new Promise() {
                @Override
                public void onSuccess() {
                    // Nothing
                }


                @Override
                public void onError(Exception ex) {
                    // Display error
                }
            });

        }

        // ENDERCHEST INVSEE
        if (inventory.getType().equals(InventoryType.ENDER_CHEST) && SyncManager.INSTANCE.isModuleEnabled("enderchest") && CommandEnd.activeInvseePlayer.keySet().contains(player.getName())) {

            // Get the module.
            EnderChestPlayerSyncModule tmpModule = null;
            try {
                tmpModule = (EnderChestPlayerSyncModule) SyncManager.INSTANCE.getModule("enderchest");
            }
            catch (ModuleNotFoundException e) {
                // TODO: Add module not enabled player message..
                return;
            }
            final EnderChestPlayerSyncModule module = tmpModule;

            // Cache the target player UUID & Remove from list.
            UUID targetPlayer = CommandEnd.activeInvseePlayer.get(player.getUniqueId());
            CommandEnd.activeInvseePlayer.remove(player.getUniqueId());

            // Update enderchest data.
            boolean res = module.setPlayerEnderChestInventory(targetPlayer, inventory);

            Main.INSTANCE.logError(Boolean.toString(res));
            System.out.println(res);

            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.INSTANCE, () -> {

                // Sync the target player.
                try {
                    SyncManager.INSTANCE.syncPlayer(targetPlayer, module, new Promise() {
                        @Override
                        public void onSuccess() {

                        }


                        @Override
                        public void onError(Exception ex) {

                        }
                    });
                } catch (NonePlayersOnline nonePlayersOnline) {
                    nonePlayersOnline.printStackTrace();
                }

            }, 20L);

        }





        // INVENTORY INVSEE
        if (CommandInv.activeInvseePlayer.keySet().contains(player.getName()) && SyncManager.INSTANCE.isModuleEnabled("inventory")) {

            /*TaskChain<?> chain = Main.newChain();
            chain
                    .async(() -> {

                        // Get the inventory module.
                        PlayerSyncModule inventoryModule = null;
                        try {
                            inventoryModule = (PlayerSyncModule) SyncManager.INSTANCE.getModule("inventory");
                        }
                        catch (ModuleExceptions.ModuleNotFoundException e) {
                            // TODO: Add module not enabled player message..
                            return;
                        }

                        Row row;
                        try {
                            row = Main.API.getPlayerDataRow(Bukkit.getOfflinePlayer(UUIDUtil.getUUID(CommandInv.activeInvseePlayer.get(player.getName()))), inventoryModule);
                        } catch (SQLException | DatabaseExceptions.RowNotFoundException e) {
                            e.printStackTrace();
                            return;
                        }

                        row.set("inventory_encoded", Serializer.INSTANCE.getInventorySerializer().inventoryToBase64(inventory));
                        row.update();

                    })
                    .sync(() -> {

                        // Get the inventory module.
                        PlayerSyncModule inventoryModule = null;
                        try {
                            inventoryModule = (PlayerSyncModule) SyncManager.INSTANCE.getModule("inventory");
                        }
                        catch (ModuleExceptions.ModuleNotFoundException e) {
                            // TODO: Add module not enabled player message..
                            return;
                        }

                        // Sync the target player.
                        Main.INSTANCE.logError(player.getName());
                        Main.INSTANCE.logError(CommandInv.activeInvseePlayer.get(player.getName()));
                        if (Bukkit.getPlayer(CommandInv.activeInvseePlayer.get(player.getName())) != null) {
                            SyncManager.INSTANCE.syncPlayer(Bukkit.getPlayer(CommandEnd.activeInvseePlayer.get(player.getName())), inventoryModule, new Promise() {
                                @Override
                                public void onSuccess() {}
                                @Override
                                public void onError(Exception ex) {}
                            });
                        }

                        // Remove the player.
                        CommandInv.activeInvseePlayer.remove(player.getName());

                    })
                    .execute();*/

        }


    }

}

