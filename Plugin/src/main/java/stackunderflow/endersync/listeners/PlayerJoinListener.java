package stackunderflow.endersync.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import stackunderflow.endersync.Main;
import stackunderflow.endersync.SyncManager;
import stackunderflow.endersync.exceptions.PlayerOccupiedException;
import stackunderflow.endersync.utils.Promise;


/*
 * Handles players joining.
 * -> Starting the sync process.
 */
public class PlayerJoinListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent event) {

        Player player = event.getPlayer();


        // Kick player when updating.
        /*if (AutoUpdater.INSTANCE.isUpdateInProgress()) {
            player.kickPlayer(new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("kickReasonUpdate")).getSTR());
            return;
        }*/

        // Send update info message when permission.
        /*if (BukkitUtils.playerHasPermission(player, "endersync.update") && AutoUpdater.INSTANCE.isShouldInformAdmin()) {
            player.sendMessage("");
            new StringFormatter("§7§l-=-=-=-=-=-= §b§lUPDATER §7§l=-=-=-=-=-=-").sendMessageTo(player);
            new StringFormatter("  §dNew update installed! (§ev{oldVersion} §d-> §ev{newVersion}§d§l)")
                    .replace("{oldVersion}", AutoUpdater.INSTANCE.getInformOldVersion())
                    .replace("{newVersion}", AutoUpdater.INSTANCE.getLatestVersionCache())
                    .sendMessageTo(player);
            new StringFormatter("  §dInstalled at: §e{timestamp}").replace("{timestamp}", AutoUpdater.INSTANCE.getInformTimestamp()).sendMessageTo(player);
            player.sendMessage("");
            new StringFormatter("  §6§lTo apply the update, please restart the server!").sendMessageTo(player);
            new StringFormatter("§7§l-=-=-=-=-=-= ------- =-=-=-=-=-=-").sendMessageTo(player);
            player.sendMessage("");
        }*/

        // Start sync.
        syncTask(player);


    }

    private void syncTask(Player player) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(Main.INSTANCE, () -> {
            SyncManager.INSTANCE.syncPlayer(player, new Promise() {

                @Override
                public void onSuccess() {

                }


                @Override
                public void onError(Exception ex) {
                    if (ex instanceof PlayerOccupiedException) {
                        syncTask(player);
                    }
                }

            });
        }, 20L * 1);
    }

}
