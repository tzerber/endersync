package stackunderflow.endersync.events;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import stackunderflow.endersync.modules.PlayerSyncModule;


/*
 * This event is called when a data save is about to be started.
 * It can be canceled to prevent the save.
 */
@Getter
@Setter
public class DataSaveStartEvent extends Event implements Cancellable {

    // ================================     VARS

    // References
    private PlayerSyncModule module;
    private Object meta;

    // State
    private boolean cancelled;
    private static final HandlerList handlers = new HandlerList();




    // ================================     CONSTRUCTOR

    public DataSaveStartEvent(PlayerSyncModule module, Object meta) {
        setModule(module);
        setMeta(meta);
        setCancelled(false);
    }


    @Override
    public boolean isCancelled() {
        return this.cancelled;
    }

    @Override
    public void setCancelled(boolean arg0) {
        this.cancelled = arg0;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
