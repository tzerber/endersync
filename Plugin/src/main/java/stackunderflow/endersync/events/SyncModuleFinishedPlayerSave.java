package stackunderflow.endersync.events;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import stackunderflow.endersync.modules.PlayerSyncModule;


/*
 * This event is called when a SyncModule finishes a player sync.
 */
@Getter
@Setter
public class SyncModuleFinishedPlayerSave extends Event implements Cancellable {

    // ================================     VARS

    // References
    private PlayerSyncModule module;
    private Player player;

    // State
    private boolean cancelled;
    private static final HandlerList handlers = new HandlerList();



    // ================================     CONSTRUCTOR

    public SyncModuleFinishedPlayerSave(Player player, PlayerSyncModule module) {
        setPlayer(player);
        setCancelled(false);
        setModule(module);
    }



    // ================================     LOGIC

    @Override
    public boolean isCancelled() {
        return this.cancelled;
    }

    @Override
    public void setCancelled(boolean arg0) {
        this.cancelled = arg0;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
    public static HandlerList getHandlerList() {
        return handlers;
    }

}
