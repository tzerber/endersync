package stackunderflow.endersync.utils;


import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.List;

/*
 * Enhanced logic of the default broadcast function.
 */
public class Broadcast {


    /*
     * Broadcast a message to all server players except for one.
     */
    public static void broadcastWithException(String message, Player except) {
        for (Player p : Bukkit.getServer().getOnlinePlayers()) {
            if (!p.getUniqueId().equals(except.getUniqueId())) {
                p.sendMessage(message);
            }
        }
    }


    /*
     * Broadcast a message to all server players except for one.
     */
    public static void broadcastWithExceptions(String message, List<Player> exceptions) {
        for (Player p : Bukkit.getServer().getOnlinePlayers()) {
            for (Player pp : exceptions) {
                if (!p.getUniqueId().equals(pp.getUniqueId())) {
                    p.sendMessage(message);
                }
            }
        }
    }


    /*
     * Broadcast a message to all players on list.
     */
    public static void broadcastTo(String message, List<Player> players) {
        for (Player p : players) {
            p.sendMessage(message);
        }
    }


    /*
     * Broadcast a message to all players.
     */
    public static void all(String message) {
        for (Player p : Bukkit.getServer().getOnlinePlayers()) {
            p.sendMessage(message);
        }
    }


}
