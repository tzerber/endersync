package stackunderflow.endersync.utils;


import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;


/*
 * Utility code for downloading stuff.
 */
public class Download {


    /*
     * Downloads a file to a specific location.
     */
    public static void file(String url, String fileName) throws MalformedURLException, IOException {
        URL _url = new URL(url);
        InputStream is = _url.openStream();
        File dest = new File(new File("plugins") + "/" + fileName);
        dest.getParentFile().mkdirs();
        dest.createNewFile();
        OutputStream os = new FileOutputStream(dest);
        byte data[] = new byte[1024];
        int count;
        while ((count = is.read(data, 0, 1024)) != -1) {
            os.write(data, 0, count);
        }
        os.flush();
        is.close();
        os.close();
    }

}
