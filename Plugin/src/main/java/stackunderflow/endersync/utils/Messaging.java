package stackunderflow.endersync.utils;


import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import stackunderflow.endersync.Main;
import stackunderflow.endersync.exceptions.NonePlayersOnline;
import stackunderflow.endersync.listeners.PluginMessageListener;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.UUID;

/*
 * Utilities for messaging between Bukkit & BungeeCord.
 */
public class Messaging {

    /*
     * Sends a message to bungeecord.
     */
    public static void sendToBungeeCord(Player player, String channel, String message){
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            out.writeUTF(channel);
            out.writeUTF(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
        player.sendPluginMessage(Main.INSTANCE, "BungeeCord", b.toByteArray());
    }


    /*
     * A simple interface to make Is-Occupied callbacks nice :)
     */
    public interface IOPCallback {
        void onTrue();
        void onFalse();
    }


    /*
     * Asks BungeeCord whether a player is occupied and runs callback when response arrives.
     */
    public static void isPlayerOccupied(UUID playerUUID, IOPCallback callback) throws NonePlayersOnline {

        // THROW: None players online!
        if (Bukkit.getOnlinePlayers().size() <= 0) throw new NonePlayersOnline("No players online to send BungeeBridge message!");

        PluginMessageListener.INSTANCE.getOccupiedPlayersCallbacks().put(playerUUID, callback);
        sendToBungeeCord((Player) Bukkit.getOnlinePlayers().toArray()[0], "IOP", playerUUID.toString());
    }
    public static void isPlayerOccupied(Player player, IOPCallback callback) {
        PluginMessageListener.INSTANCE.getOccupiedPlayersCallbacks().put(player.getUniqueId(), callback);
        sendToBungeeCord(player, "IOP", player.getUniqueId().toString());
    }

}
