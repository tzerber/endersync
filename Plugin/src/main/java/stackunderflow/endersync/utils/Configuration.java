package stackunderflow.endersync.utils;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import stackunderflow.endersync.Main;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;


/*
 * A simple interface for handling config files.
 */
@Getter
@Setter
public class Configuration {

    // ============== VARS

    // References
    public static Configuration INSTANCE;
    private Plugin plugin;

    // Settings
    private String defaultConfig;
    private HashMap<String, YamlConfiguration> loadedConfigs;
    private HashMap<String, File> loadedConfigPaths;


    // ============== CONSTRUCTOR

    public Configuration(Plugin plugin) {
        Configuration.INSTANCE = this;
        setPlugin(plugin);
        setLoadedConfigs(new HashMap<>());
        setLoadedConfigPaths(new HashMap<>());
    }



    // ============== CONFIG LOGIC

    /*
     * Loads a config from a file. (Creates it if it does not exist).
     */
    public void loadConfigFile(String configName, File file) throws IOException, InvalidConfigurationException {
        loadConfigFile(configName, file, true);
    }
    public void loadConfigFile(String configName, File file, boolean override) throws IOException, InvalidConfigurationException {

        // Create if not exist.
        if (!file.exists()) createConfig(file, override);

        YamlConfiguration config = new YamlConfiguration();

        // Crate new nodes from default resource.
        if (getPlugin().getResource(file.getName()) != null) {

            // Rename existing config file.
            File oldConfigFile = new File(getPlugin().getDataFolder() + File.separator + file.getName().split("\\.")[0]+"-tmp"+file.getName().split("\\.")[1]);
            file.renameTo(oldConfigFile);

            // Load old config.
            config.load(oldConfigFile);

            // Save default resource.
            getPlugin().saveResource(file.getName(), true);
            YamlConfiguration cfg = new YamlConfiguration();
            cfg.load(file);
            saveDefaults(config, cfg);
            config.save(file);

            // Delete tmp resource.
            oldConfigFile.delete();

        }
        else {

            // Load.
            config.load(file);

        }


        // Save.
        getLoadedConfigs().put(configName, config);
        getLoadedConfigPaths().put(configName, file);
        saveConfig(configName);

    }

    /*
     * Saves the default values from one config to another.
     */
    private void saveDefaults(YamlConfiguration config, YamlConfiguration defaultConfig) {
        saveDefaults("", config, defaultConfig);
    }
    private void saveDefaults(String root, YamlConfiguration config, YamlConfiguration defaultConfig) {
        ConfigurationSection section = defaultConfig.getConfigurationSection(root);
        if (section != null) {
            for (String key : section.getKeys(false)) {
                if (config.get(root+"."+key, null) == null) config.set(root+"."+key, defaultConfig.get(root+"."+key));
                if (root.equals("")) saveDefaults(key, config, defaultConfig);
                else saveDefaults(root+"."+key, config, defaultConfig);
            }
        }
    }


    /*
     * Reloads a config.
     */
    public void reloadConfig() throws IOException, InvalidConfigurationException { reloadConfig(getDefaultConfig()); }
    public void reloadConfig(String configName) throws IOException, InvalidConfigurationException {

        // RET: Not found.
        if (get(configName) == null) return;

        // Unload.
        File path = getLoadedConfigPaths().get(configName);
        getLoadedConfigs().remove(configName);
        getLoadedConfigPaths().remove(configName);

        // Load.
        loadConfigFile(configName, path);

    }


    /*
     * Creates a config file.
     */
    public void createConfig(File file, boolean override) throws IOException {

        // Create folders.
        file.getParentFile().mkdirs();

        // Create file.
        if (override) {
            if (getPlugin().getResource(file.getName()) != null) {
                getPlugin().saveResource(file.getName(), true);
            }
            else file.createNewFile();
        }
        else file.createNewFile();


    }


    /*
     * Saves a config by name.
     */
    public void saveConfig(String configName) throws IOException {

        // RET: Not found.
        if (!getLoadedConfigs().keySet().contains(configName)) return;

        // Save.
        FileConfiguration config = getLoadedConfigs().get(configName);
        config.save(getLoadedConfigPaths().get(configName));

    }
    public void saveAll() throws IOException {
        for (String key : getLoadedConfigs().keySet()) saveConfig(key);
    }


    /*
     * Gets a loaded config.
     */
    public YamlConfiguration get() { return getLoadedConfigs().get(getDefaultConfig()); }
    public YamlConfiguration get(String configName) {
        return getLoadedConfigs().get(configName);
    }


    /*
     * Gets a message if it exists.
     */
    public String getLocalizedMessage(String path) { return getLocalizedMessage("lang", path); }
    public String getLocalizedMessage(String configName, String path) {

        // Get config.
        YamlConfiguration config = getLoadedConfigs().get(configName);

        // RET: Config not found.
        if (config == null) return "§cERROR: Config '§e"+configName+"§c' not found!";

        // Get message.
        String msg = config.getString("messages."+path);

        if (msg == null || msg.equals("")) return null;
        // RET: Message not found.
        //if (msg == null || msg.equals("")) return "§cERROR: Message '§e"+path+"§c' empty or not found!";

        return msg;

    }

    /*
     * Gets a location from config.
     */
    public Location getLocation(String path) { return getLocation(getDefaultConfig(), path); }
    public Location getLocation(String configName, String path) {

        // Get config.
        YamlConfiguration config = getLoadedConfigs().get(configName);

        // RET: Config not found.
        if (config == null) return null;

        // Assemble location.
        String world = config.getString(path + ".world");
        double x = config.getDouble(path + ".x");
        double y = config.getDouble(path + ".y");
        double z = config.getDouble(path + ".z");
        double yaw = config.getDouble(path + ".yaw");
        double pitch = config.getDouble(path + ".pitch");

        return new Location(Bukkit.getWorld(world), x, y, z, (float) yaw, (float) pitch);

    }


    /*
     * Sets a location in config.
     */
    public void setLocation(String path, Location location) { setLocation(getDefaultConfig(), path, location); }
    public void setLocation(String configName, String path, Location location) {

        // Get config.
        YamlConfiguration config = getLoadedConfigs().get(configName);

        // RET: Config not found.
        if (config == null) return;

        // Save location.
        config.set(path + ".world", location.getWorld().getName());
        config.set(path + ".x", location.getX());
        config.set(path + ".y", location.getY());
        config.set(path + ".z", location.getZ());
        config.set(path + ".yaw", location.getYaw());
        config.set(path + ".pitch", location.getPitch());

    }

}
