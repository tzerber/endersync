package stackunderflow.endersync.utils;

/*
 * Util logic for handling versions.
 */
public class VersionUtil {

    /*
     * Return true if v1 is newer than v2.
     */
    public static boolean isNewer(String v1, String v2) {

        String[] v1Parts = v1.split("\\.");
        String[] v2Parts = v2.split("\\.");
        if (Integer.parseInt(v1Parts[0]) > Integer.parseInt(v2Parts[0])) return true;
        if (Integer.parseInt(v1Parts[0]) == Integer.parseInt(v2Parts[0])) {
            if (Integer.parseInt(v1Parts[1]) > Integer.parseInt(v2Parts[1])) return true;
            if (Integer.parseInt(v1Parts[1]) == Integer.parseInt(v2Parts[1])) {
                if (Integer.parseInt(v1Parts[2]) > Integer.parseInt(v2Parts[2])) return true;
            }
        }
        return false;

    }

}
