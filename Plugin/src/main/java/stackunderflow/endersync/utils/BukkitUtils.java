package stackunderflow.endersync.utils;


import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import stackunderflow.endersync.Main;

/*
 * Contains utility logic for bukkit stuff.
 */
public class BukkitUtils {

    /*
     * Calls the event and returns whether is was cancelled.
     */
    public static boolean call(Event event) {
        Bukkit.getPluginManager().callEvent(event);
        if (event instanceof Cancellable) {
            return !((Cancellable) event).isCancelled();
        }
        Main.INSTANCE.logError("Encountered event which is not cancellable inside of EventUtils! Please contact developer!");
        Main.INSTANCE.logError("EventName: "+event.getEventName());
        return false;
    }


    /*
     * Checks whether a player is op or has the permission.
     */
    public static boolean playerHasPermission(Player player, String permission) {
        return player.isOp() || player.hasPermission(permission);
    }

}
