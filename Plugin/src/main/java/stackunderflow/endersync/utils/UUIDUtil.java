package stackunderflow.endersync.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mojang.util.UUIDTypeAdapter;
import lombok.Getter;
import lombok.Setter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.UUID;


/*
 * Some basic Mojang API wrapper methods, for converting / retrieving player UUID's & usernames.
 */
@Getter
@Setter
public class UUIDUtil {

    // ================================     VARS

    // References
    public static final long FEBRUARY_2015 = 1422748800000L;
    private static Gson gson = new GsonBuilder().registerTypeAdapter(UUID.class, new UUIDTypeAdapter()).create();

    // Settings
    private static final String UUID_URL = "https://api.mojang.com/users/profiles/minecraft/%s?at=%d";
    private static final String NAME_URL = "https://api.mojang.com/user/profiles/%s/names";

    // GSON Serialization properties
    private String name;
    private UUID id;


    // ================================     UTILITY LOGIC


    /*
     * Get the current UUID of a named player.
     */
    public static UUID getUUID(String name) {
        return getUUIDAt(name, System.currentTimeMillis());
    }


    /*
     * Get the UUID of a named player at a given timestamp.
     */
    public static UUID getUUIDAt(String name, long timestamp) {

        try {

            // Make HTTP request.
            HttpURLConnection connection = (HttpURLConnection) new URL(String.format(UUID_URL, name, timestamp/1000)).openConnection();
            connection.setReadTimeout(5000);

            // Parse & Return response.
            UUIDUtil data = gson.fromJson(new BufferedReader(new InputStreamReader(connection.getInputStream())), UUIDUtil.class);
            if (data == null) return null;
            return data.getId();

        } catch (MalformedURLException muEX) {
            muEX.printStackTrace();
        }
        catch (IOException ioEX) {
            ioEX.printStackTrace();
        }

        return null;

    }


    /*
     * Get the username associated with a player's UUID.
     */
    public static String getName(UUID uuid) {

        try {

            // Make HTTP request.
            HttpURLConnection connection = (HttpURLConnection) new URL(String.format(NAME_URL, UUIDTypeAdapter.fromUUID(uuid))).openConnection();
            connection.setReadTimeout(5000);

            // Parse & Return response.
            UUIDUtil[] nameHistory = gson.fromJson(new BufferedReader(new InputStreamReader(connection.getInputStream())), UUIDUtil[].class);
            UUIDUtil currentNameData = nameHistory[nameHistory.length - 1];
            return currentNameData.name;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

}
