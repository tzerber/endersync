package stackunderflow.endersync.utils;

public interface Promise {
    void onSuccess();
    void onError(Exception ex);
}
