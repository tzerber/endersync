package stackunderflow.endersync.exceptions;

import lombok.Getter;
import lombok.Setter;

/*
 * A base class from which all other exceptions derive.
 */
@Getter
@Setter
public abstract class BaseException extends Exception {

    private String message;

    public BaseException(String message) {
        setMessage(message);
    }

}
