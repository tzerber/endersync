package stackunderflow.endersync.exceptions;

// ModuleNotFoundException
public class ModuleNotFoundException extends BaseException {
    public ModuleNotFoundException(String message) {
        super(message);
    }
}
