package stackunderflow.endersync.exceptions;

// RowNotFoundException
public class RowNotFoundException extends BaseException {
    public RowNotFoundException(String message) {
        super(message);
    }
}
