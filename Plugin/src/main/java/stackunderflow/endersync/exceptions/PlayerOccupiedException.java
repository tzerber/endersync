package stackunderflow.endersync.exceptions;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;


/*
 * Thrown when an action is canceled because the player is still occupied.
 */
@Getter
@Setter
public class PlayerOccupiedException extends BaseException {
    private Player player;
    public PlayerOccupiedException(String message, Player player) {
        super(message);
        setPlayer(player);
    }
}
