package stackunderflow.endersync;

import co.aikar.taskchain.BukkitTaskChainFactory;
import co.aikar.taskchain.TaskChain;
import co.aikar.taskchain.TaskChainFactory;
import lombok.Getter;
import lombok.Setter;
import net.milkbowl.vault.economy.Economy;
import org.apache.commons.io.FileUtils;
import org.bukkit.World;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import stackunderflow.endersync.api.EnderSyncAPI;
import stackunderflow.endersync.commands.*;
import stackunderflow.endersync.commands.administration.CommandDelete;
import stackunderflow.endersync.commands.administration.CommandReload;
import stackunderflow.endersync.commands.manipulation.*;
import stackunderflow.endersync.database.mysql.DatabaseManager;
import stackunderflow.endersync.database.mysql.Row;
import stackunderflow.endersync.exceptions.ModuleExceptions;
import stackunderflow.endersync.exceptions.ModuleNotFoundException;
import stackunderflow.endersync.exceptions.RowNotFoundException;
import stackunderflow.endersync.listeners.*;
import stackunderflow.endersync.modules.*;
import stackunderflow.endersync.utils.Configuration;
import stackunderflow.endersync.utils.Promise;
import stackunderflow.endersync.utils.StringFormatter;
import stackunderflow.endersync.serializers.Serializer;
import stackunderflow.endersync.utils.VersionUtil;
import stackunderflow.libs.commandfactory.CommandFactory;

import java.io.*;
import java.sql.SQLException;
import java.util.List;

import static org.bukkit.Bukkit.getScheduler;


@Getter
@Setter
public class Main extends JavaPlugin {

    // ================================     VARS

    // References
    public static Main INSTANCE;
    public static EnderSyncAPI API;
    public ConsoleCommandSender console = getServer().getConsoleSender();
    private static TaskChainFactory taskChainFactory;

    // Settings
    private boolean DEBUG = false;
    private String pluginChatPrefix = "§l§7» §l§3Ender§l§fSync {success}§l§7*";
    private String version;
    public static String resourceID = "64344";
    public boolean vault_sync_enable = false;



    // ================================     CONSTRUCTOR & SINGLETON

    public Main() {
        if (Main.INSTANCE == null) {
            Main.INSTANCE = this;
        }
    }



    // ================================     BUKKIT OVERRIDES

    @Override
    public void onEnable() {

        // Setup plugin messaging.
        this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        this.getServer().getMessenger().registerIncomingPluginChannel(this, "endersync:return", new PluginMessageListener());

        // Setup TaskChain.
        taskChainFactory = BukkitTaskChainFactory.create(this);

        // Load config & clear info paths.
        new Configuration(this);

        try {
            Configuration.INSTANCE.loadConfigFile("config", new File(getDataFolder() + File.separator + "config.yml"), true);
        } catch (IOException e) {
            logError("Could not find config.yml!");
            if (isDEBUG()) e.printStackTrace();
            setEnabled(false);
            return;
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
            setEnabled(false);
            return;
        }

        Configuration.INSTANCE.setDefaultConfig("config");

        try {
            Configuration.INSTANCE.loadConfigFile("features", new File(getDataFolder() + File.separator + "features.yml"), true);
        } catch (IOException e) {
            logError("Could not find features.yml!");
            if (isDEBUG()) e.printStackTrace();
            setEnabled(false);
            return;
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
            setEnabled(false);
            return;
        }

        try {
            Configuration.INSTANCE.loadConfigFile("lang", new File(getDataFolder() + File.separator + "lang-"+Configuration.INSTANCE.get().getString("plugin.lang")+".yml"), true);
        } catch (IOException e) {
            logError("Could not find language file: "+getDataFolder() + File.separator + "lang-"+Configuration.INSTANCE.get().getString("plugin.lang")+".yml");
            if (isDEBUG()) e.printStackTrace();
            setEnabled(false);
            return;
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
            setEnabled(false);
            return;
        }

        getConsole().sendMessage("");
        log("Enabling EnderSync plugin v"+getDescription().getVersion());
        getConsole().sendMessage("");

        // Setup.
        setVersion(getDescription().getVersion());
        setPluginChatPrefix(Configuration.INSTANCE.get().getString("plugin.chatPrefix", "§l§7» §l§3Ender§l§fSync {success}§l§7*"));
        setDEBUG(Configuration.INSTANCE.get().getBoolean("plugin.debug", false));
        new SyncManager();
        new Serializer();

        // Check version.
        if (Serializer.INSTANCE.isCompatibleVersion()) {
            Main.INSTANCE.log("Running on supported server version ("+Serializer.INSTANCE.getVersion()+")");
            Serializer.INSTANCE.init();
        }
        else {
            log("");
            logError("Unsupported server version: "+Serializer.INSTANCE.getVersion());
            log("");
            setEnabled(false);
            return;
        }

        // Debug.
        logDebug("Debug enabled!");

        log("Setting up MySQL connection...");

        // Setup MySql. RET: If error disable plugin.
        if (!setupMySQL()) {
            setEnabled(false);
            return;
        }

        log("Enabling features & default modules...");

        if (!enableDefaultModules()) { setEnabled(false); return; }
        enableFeatures();

        log("Registering commands...");

        // Register commands.
        CommandFactory cmdFac = new CommandFactory();

        // Base info.
        cmdFac.addCommand(new CommandBase("base"));
        cmdFac.addCommand(new CommandHelp("help"));

        // Admin commands.
        cmdFac.addCommand(new CommandReload("reload"));
        cmdFac.addCommand(new CommandSaveAndKick("saveAndKick"));
        //cmdFac.addCommand(new CommandUpdate("es importData <name>"));
        //cmdFac.addCommand(new CommandUpdate("es backup"));

        // Manipulation commands.
        cmdFac.addCommand(new CommandDelete("delete"));
        cmdFac.addCommand(new CommandInv("inv"));
        //cmdFac.addCommand(new CommandInv("es armor <name>"));
        cmdFac.addCommand(new CommandEnd("end"));
        cmdFac.addCommand(new CommandEcoBal("ecoBal"));
        cmdFac.addCommand(new CommandEcoSet("ecoSet"));
        cmdFac.addCommand(new CommandGetXP("getXP"));
        cmdFac.addCommand(new CommandSetXP("setXP"));
        //cmdFac.addCommand(new CommandInv("es addXp <name> <exp>"));

        cmdFac.addCommand(new CommandSync("sync"));
        cmdFac.addCommand(new CommandSave("save"));

        this.getCommand("endersync").setExecutor(cmdFac);
        this.getCommand("es").setExecutor(cmdFac);
        this.getCommand("sync").setExecutor(cmdFac);
        this.getCommand("save").setExecutor(cmdFac);

        log("Registering listeners...");

        // Register listeners.
        getServer().getPluginManager().registerEvents(new PlayerJoinListener(), this);
        getServer().getPluginManager().registerEvents(new PlayerQuitListener(), this);
        getServer().getPluginManager().registerEvents(new CommandPreprocessListener(), this);
        getServer().getPluginManager().registerEvents(new InventoryClickListener(), this);
        getServer().getPluginManager().registerEvents(new InventoryCloseListener(), this);
        getServer().getPluginManager().registerEvents(new DropItemListener(), this);
        getServer().getPluginManager().registerEvents(new SyncModuleFinishedPlayerSaveListener(), this);

        // Enable API.
        Main.API = new EnderSyncAPI();

        // Do upgrading.
        doUpgrading();

        log("Plugin is Ready! :-)");
        getConsole().sendMessage("");

    }


    @Override
    public void onDisable() {

        getConsole().sendMessage("");
        log("Disabling EnderSync plugin v"+getVersion());

        // Save config.
        try {
            Configuration.INSTANCE.saveAll();
        } catch (IOException e) {
            Main.INSTANCE.logError("Could not save config files!");
            e.printStackTrace();
        }

        // Close db connection.
        try {
            if (DatabaseManager.INSTANCE != null) DatabaseManager.INSTANCE.closeConnection();
        } catch (SQLException e) {
            logError("Could not close MySQL connection!");
            e.printStackTrace();
        }

        // Delete dat files if enabled.
        if (Configuration.INSTANCE.get("features").getBoolean("features.cleanupUnnecessaryFiles.enabled")) {
            for (World world : getServer().getWorlds()) {
                //System.out.println(world.getWorldFolder().getPath()+"/playerdata");
                logDebug("Deleting playerData for world: "+world.getName());
                try {
                    FileUtils.deleteDirectory(new File(world.getWorldFolder().getPath()+"/playerdata"));
                } catch (IOException e) {
                    logError("Could not delete playerData for world: "+world.getName());
                    e.printStackTrace();
                }
            }
        }

        getConsole().sendMessage("");

    }



    // ================================     LOGIC

    /*
     * Sets up the mysql stuff.
     */
    private boolean setupMySQL() {

        // Register jdbc
        try {
            Class.forName("com.mysql.jdbc.Driver");
        }
        catch (ClassNotFoundException e) {
            logError("MySQL Connection error: Could not find Class com.mysql.jdbc.Driver");
            return false;
        }

        // Create DatabaseManager.
        new DatabaseManager();

        // Establish db connection.
        try {
            DatabaseManager.INSTANCE.openConnection(
                    Configuration.INSTANCE.get().getString("database.host", "127.0.0.1"),
                    Integer.toString(Configuration.INSTANCE.get().getInt("database.port", 3306)),
                    Configuration.INSTANCE.get().getString("database.databaseName", "endersync"),
                    Configuration.INSTANCE.get().getString("database.user", "enderSyncUser"),
                    Configuration.INSTANCE.get().getString("database.password", "enderSync!123")
            );
        } catch (SQLException e) {
            logError("Could not connect to the MySQL Database! Please make sure the credentials & database name are correct!");
            logError("Error cause: "+e.getMessage());
            if (isDEBUG()) {
                e.printStackTrace();
            }
            else {
                getConsole().sendMessage("");
                log("§bJust installed the plugin?");
                log("§fHey there, It seems like you have just installed the plugin.");
                log("§fPlease add your database credentials inside of the 'plugins/config.yml' file.");
                log("§fand restart the server.");
                log("§7Need help? Discord: §ehttps://discord.gg/YFArBG7 §7/ Resource page -> Tutorial (§ehttps://www.spigotmc.org/resources/endersync-sync-playerdata-between-servers-inventory-health-xp.64344/§7)");
                getConsole().sendMessage("");
            }
            return false;
        }

        return true;

    }


    /*
     * Enables the default modules based on whether tey are enabled or not in the config.
     */
    private boolean enableDefaultModules() {

        try {

            SyncManager.INSTANCE.enableModule(new InventoryPlayerSyncModule());
            SyncManager.INSTANCE.enableModule(new EnderChestPlayerSyncModule());
            SyncManager.INSTANCE.enableModule(new ArmorPlayerSyncModule());
            SyncManager.INSTANCE.enableModule(new GameModePlayerSyncModule());
            SyncManager.INSTANCE.enableModule(new ExperiencePlayerSyncModule());
            SyncManager.INSTANCE.enableModule(new PotionEffectsPlayerSyncModule());
            SyncManager.INSTANCE.enableModule(new HealthPlayerSyncModule());
            SyncManager.INSTANCE.enableModule(new FoodPlayerSyncModule());
            SyncManager.INSTANCE.enableModule(new AirPlayerSyncModule());
            SyncManager.INSTANCE.enableModule(new LocationPlayerSyncModule());
            SyncManager.INSTANCE.enableModule(new FlightPlayerSyncModule());
            SyncManager.INSTANCE.enableModule(new LastSeenPlayerSyncModule());

            if (Configuration.INSTANCE.get("features").getBoolean("features.sync.vault.enabled") || Configuration.INSTANCE.get("features").getBoolean("features.save.vault.enabled")) {
                // Setup VaultAPI.
                RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(Economy.class);
                if (economyProvider != null) {
                    SyncManager.INSTANCE.enableModule(new VaultPlayerSyncModule(economyProvider.getProvider()));
                    vault_sync_enable = true;
                    log("  - VaultSync: Using economy system: "+ economyProvider.getProvider().getName());
                }
                else {
                    logError("Module 'vault' is enabled but no Vault compatible economy system could be found!");
                }
            }

        }
        catch (ModuleExceptions.ModuleAlreadyEnabledException e) {
            logError("Module '"+e.getModule().getName()+"' is already enabled!");
            logError("This error should not have occurred in 'Main'! Please contact the developer!");
            return false;
        }
        catch (ModuleExceptions.InvalidModuleException e) {
            logError("Module '"+e.getModule().getName()+"' is invalid!");
            logError("This error should not have occurred in 'Main'! Please contact the developer!");
            return false;
        }
        return true;

    }


    /*
     * Enables other configurable features like autoSave.
     */
    private void enableFeatures() {

        // AutoSave.
        if (Configuration.INSTANCE.get("features").getBoolean("features.autoSaveData.enabled")) {
            getServer().getScheduler().scheduleSyncRepeatingTask(this, () -> {
                for (Player player : getServer().getOnlinePlayers()) {
                    SyncManager.INSTANCE.savePlayer(player, new Promise() {
                        @Override
                        public void onSuccess() {}
                        @Override
                        public void onError(Exception ex) {}
                    });
                }
            }, 0L, 20L * 60 * Configuration.INSTANCE.get("features").getInt("features.autoSaveData.interval"));
        }


        // CleanupInactive.
        if (Configuration.INSTANCE.get("features").getBoolean("features.cleanupInactive.enabled") && SyncManager.INSTANCE.isModuleEnabled("lastSeen")) {
            long interval = isDEBUG() ? 20L * 60 : 20L * 60 * 10;
            getScheduler().scheduleSyncRepeatingTask(this, () -> {

                // TODO: Make async.

                // Get the last_seen module.
                try {

                    final PlayerSyncModule lastSeenModule = (PlayerSyncModule) SyncManager.INSTANCE.getModule("lastSeen");
                    String compareTimestamp = Long.toString(System.currentTimeMillis() - (1000 * 60 * 60 * 24 * Configuration.INSTANCE.get("features").getInt("features.cleanupInactive.deleteAccountAfterXDays")));
                    List<Row> clearPlayersRows = lastSeenModule.getTableManager().getRows("SELECT * FROM es_module_lastSeen WHERE last_seen<"+compareTimestamp);
                    for (Row row : clearPlayersRows) {

                        String playerUUUID = (String) row.get("player_uuid");
                        for (PlayerSyncModule module : SyncManager.INSTANCE.getActivePlayerModules().values()) {
                            Row modRow = module.getTableManager().getRow("player_uuid", playerUUUID);
                            modRow.delete();
                        }
                        row.delete();

                    }

                }
                catch (ModuleNotFoundException e) {
                    Main.INSTANCE.logError("This should not have happened! Could not get lastSeen module even though it is enabled!");
                    e.printStackTrace();
                    return;
                }
                catch (RowNotFoundException e) {
                    Main.INSTANCE.logError("RowNotFoundException whilst cleaning up inactive players.");
                    return;
                }
                catch (SQLException e) {
                    Main.INSTANCE.logError("SQL whilst cleaning up inactive players.");
                    e.printStackTrace();
                    return;
                }


            }, 0L, interval);
        }

    }


    /*
     * Upgrades stuff like database tables if necessary.
     */
    private void doUpgrading() {

        // Check version.
        if (VersionUtil.isNewer(getVersion(), "3.3.2")) {

            Main.INSTANCE.logDebug("Starting upgrading process...");

            for (PlayerSyncModule module : SyncManager.INSTANCE.getActivePlayerModules().values()) {
                try {
                    DatabaseManager.INSTANCE.executeUpdate("ALTER TABLE "+module.getTableManager().getTableName()+" DROP synced;");
                } catch (SQLException e) {
                    if (!e.getMessage().contains("exists")) {
                        Main.INSTANCE.logError("Could not upgrade! Please contact the developer!");
                        e.printStackTrace();
                        setEnabled(false);
                        return;
                    }
                }
            }

        }

    }


    // ================================     TASK CHAIN

    public static <T> TaskChain<T> newChain() {
        return taskChainFactory.newChain();
    }
    public static <T> TaskChain<T> newSharedChain(String name) {
        return taskChainFactory.newSharedChain(name);
    }



    // ================================     HELPERS

    /*
     * Output formatted log information to the console.
     */
    public void log(String str) {
        str = "{prefix} §r" + str;
        getConsole().sendMessage(new StringFormatter(str).getSTR());
    }
    public void logError(String str) {
        str = "{prefix} §l§cERROR: §r" + str;
        getConsole().sendMessage(new StringFormatter(str).getSTR());
    }
    public void logWarning(String str) {

        // RET: No debug.
        if (!isDEBUG()) return;

        str = "{prefix} §l§eWARNING: §r" + str;
        getConsole().sendMessage(new StringFormatter(str).getSTR());

    }
    public void logDebug(String str) {

        // RET: No debug.
        if (!isDEBUG()) return;

        str = "{prefix} §l§6DEBUG: §r" + str;
        getConsole().sendMessage(new StringFormatter(str).getSTR());

    }



}
