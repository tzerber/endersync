package stackunderflow.endersync.modules;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;
import stackunderflow.endersync.database.mysql.Row;
import stackunderflow.endersync.utils.Configuration;


/*
 * This sync module handles the following aspects:
 * - Health
 */
@Getter
@Setter
public class HealthPlayerSyncModule extends PlayerSyncModule {

    // ================================     VARS

    // References
    // Foo


    // Settings
    // Foo




    // ================================     CONSTRUCTOR

    public HealthPlayerSyncModule() {

        // Setup SyncModule.
        super("health");

        // Setup database tables.
        getTableManager().addColumn("player_uuid", "VARCHAR(36) NOT NULL UNIQUE");
        getTableManager().addColumn("player_name", "VARCHAR(16) NOT NULL");
        getTableManager().addColumn("health_value", "DOUBLE NOT NULL");
        getTableManager().addColumn("health_scale", "DOUBLE NOT NULL");
        getTableManager().addColumn("health_max", "DOUBLE NOT NULL");

        // Finish the init. ! This needs to be here!
        finishModuleInit();

    }



    // ================================     LOGIC


    /*
     * Implement logic to sync player data.
     */
    @Override
    public boolean onPlayerSync(Row row, Player player) {

        // RET: Sync disabled.
        if (!Configuration.INSTANCE.get("features").getBoolean("features.sync.health.enabled")) return true;

        // RET: No entry found: Call save & Return.
        if (row.isEmptyIgnorePlayerData()) { onPlayerSave(row, player); return true; }

        // Sync data.
        player.setMaxHealth((double) row.get("health_max"));
        player.setHealthScale((double) row.get("health_scale"));
        player.setHealth((double) row.get("health_value"));

        return true;

    }


    /*
     * Implement logic to save player data.
     */
    @Override
    public boolean onPlayerSave(Row row, Player player) {

        // RET: Save disabled.
        if (!Configuration.INSTANCE.get("features").getBoolean("features.save.health.enabled")) return true;

        // Save data.
        row.set("health_max", player.getMaxHealth());
        row.set("health_scale", player.getHealthScale());
        row.set("health_value", player.getHealth());
        row.update();

        return true;

    }

}
