package stackunderflow.endersync.modules;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;
import stackunderflow.endersync.Main;
import stackunderflow.endersync.SyncManager;
import stackunderflow.endersync.database.mysql.Row;
import stackunderflow.endersync.database.mysql.TableManager;
import stackunderflow.endersync.exceptions.ModuleNotFoundException;
import stackunderflow.endersync.exceptions.RowNotFoundException;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


/*
 * The base class for all player sync modules which can be extended to
 * create custom modules which can sync other player related data.
 */
@Getter
@Setter
public class PlayerSyncModule extends SyncModule {

    // ================================     VARS

    // References
    private TableManager tableManager;


    // Settings
    private String name = "default";
    private boolean valid = false;


    // State
    private boolean syncing = false;
    private List<String> blockedPlayers;



    // ================================     CONSTRUCTOR

    public PlayerSyncModule(String name) {
        setName(name);
        setTableManager(new TableManager("es_module_"+getName()));
        setBlockedPlayers(new ArrayList<>());
    }



    // ================================     INTERNAL LOGIC


    /*
     * Finishes the module initialization.
     */
    public void finishModuleInit() {
        try {
            getTableManager().createTable();
            setValid(true);
        } catch (SQLException e) {
            setValid(false);
            Main.INSTANCE.logError("Could not create MySQL table for module: "+getName()+"! Disabling it!");
            e.printStackTrace();
            try {
                SyncManager.INSTANCE.disableModule(getName());
            }
            catch (ModuleNotFoundException ex) {
                // This cannot happen
            }
        }
    }


    /*
     * Handle blocked player management.
     */
    public void blockPlayer(Player player) {
        getBlockedPlayers().add(player.getUniqueId().toString());
    }
    public void unblockPlayer(Player player) {
        getBlockedPlayers().remove(player.getUniqueId().toString());
    }
    public boolean isPlayerBlocked(Player player) {
        return getBlockedPlayers().contains(player.getUniqueId().toString());
    }



    // ================================     LOGIC


    /*
     * Get the players database row.
     */
    public Row getPlayerRow(Player player) throws SQLException, RowNotFoundException {
        return getPlayerRow(player.getUniqueId());

    }
    public Row getPlayerRow(UUID playerUUID) throws SQLException, RowNotFoundException {
        return getTableManager().getPlayerRow(playerUUID);
    }


    /*
     * Implement logic to sync player data.
     * Note: The API automatically fetches the users ROW from the db.
     */
    public boolean onPlayerSync(Row row, Player player) {
        return true;
    }


    /*
     * Implement logic to save player data.
     * Note: The API automatically fetches the users ROW from the db.
     */
    public boolean onPlayerSave(Row row, Player player) {
        return true;
    }


    /*
     * Implement logic to sync general data.
     * Example Usage: inside of onEnable of shop plugin to load / sync all created shops.
     */
    public boolean onDataSync(Object meta) { return true; }


    /*
     * Implement logic to save general data.
     * Example Usage: when a new shop is created to save all configuration.
     */
    public boolean onDataSave(Object meta) { return true; }

}
