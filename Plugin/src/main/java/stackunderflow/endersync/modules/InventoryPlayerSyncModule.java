package stackunderflow.endersync.modules;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import stackunderflow.endersync.database.mysql.Row;
import stackunderflow.endersync.serializers.Serializer;
import stackunderflow.endersync.utils.Configuration;


/*
 * This sync module handles the following aspects:
 * - Inventory
 */
@Getter
@Setter
public class InventoryPlayerSyncModule extends PlayerSyncModule {

    // ================================     VARS

    // References
    // Foo


    // Settings
    // Foo




    // ================================     CONSTRUCTOR

    public InventoryPlayerSyncModule() {

        // Setup SyncModule.
        super("inventory");

        // Setup database tables.
        getTableManager().addColumn("player_uuid", "VARCHAR(36) NOT NULL UNIQUE");
        getTableManager().addColumn("player_name", "VARCHAR(16) NOT NULL");
        getTableManager().addColumn("inventory_encoded", "TEXT NOT NULL");
        getTableManager().addColumn("hotbar_slot", "TINYINT NOT NULL");

        // Finish the init. ! This needs to be here!
        finishModuleInit();

    }



    // ================================     LOGIC


    @Override
    public void blockPlayer(Player player) {
        //player.setWalkSpeed(0);
        super.blockPlayer(player);
    }


    @Override
    public void unblockPlayer(Player player) {
        //player.setWalkSpeed(0.2);
        super.unblockPlayer(player);
    }


    /*
     * Implement logic to sync player data.
     */
    @Override
    public boolean onPlayerSync(Row row, Player player) {

        // RET: Sync disabled.
        if (!Configuration.INSTANCE.get("features").getBoolean("features.sync.inventory.enabled")) return true;

        // RET: No entry found: Call save & Return.
        if (row.isEmptyIgnorePlayerData()) { onPlayerSave(row, player); return true; }

        // Deserialize the inventory.
        Inventory inventory = Serializer.INSTANCE.getInventorySerializer().inventoryFromBase64((String) row.get("inventory_encoded"), "", InventoryType.PLAYER);

        // RET: No inv.
        if (inventory == null) return false;

        // Sync data.
        player.getInventory().setContents(inventory.getContents());
        player.getInventory().setHeldItemSlot((int) row.get("hotbar_slot"));

        return true;

    }


    /*
     * Implement logic to save player data.
     */
    @Override
    public boolean onPlayerSave(Row row, Player player) {

        // RET: Save disabled.
        if (!Configuration.INSTANCE.get("features").getBoolean("features.save.inventory.enabled")) return true;

        // RET: Sync when in creative is disabled.
        if (!Configuration.INSTANCE.get("features").getBoolean("features.save.inventory.saveWhenInCreative") && player.getGameMode().equals(GameMode.CREATIVE)) return true;

        // Save data.
        row.set("inventory_encoded", Serializer.INSTANCE.getInventorySerializer().inventoryToBase64(player.getInventory()));
        row.set("hotbar_slot", player.getInventory().getHeldItemSlot());
        row.update();

        return true;

    }

}
