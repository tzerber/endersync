package stackunderflow.endersync.modules;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import stackunderflow.endersync.database.mysql.Row;
import stackunderflow.endersync.utils.Configuration;


/*
 * This sync module handles the following aspects:
 * - GameMode
 */
@Getter
@Setter
public class GameModePlayerSyncModule extends PlayerSyncModule {

    // ================================     VARS

    // References
    // Foo


    // Settings
    // Foo




    // ================================     CONSTRUCTOR

    public GameModePlayerSyncModule() {

        // Setup SyncModule.
        super("gamemode");

        // Setup database tables.
        getTableManager().addColumn("player_uuid", "VARCHAR(36) NOT NULL UNIQUE");
        getTableManager().addColumn("player_name", "VARCHAR(16) NOT NULL");
        getTableManager().addColumn("gamemode_value", "TINYINT NOT NULL");

        // Finish the init. ! This needs to be here!
        finishModuleInit();

    }



    // ================================     LOGIC


    /*
     * Implement logic to sync player data.
     */
    @Override
    public boolean onPlayerSync(Row row, Player player) {

        // RET: Sync disabled.
        if (!Configuration.INSTANCE.get("features").getBoolean("features.sync.gameMode.enabled")) return true;

        // RET: No entry found: Call save & Return.
        if (row.isEmptyIgnorePlayerData()) { onPlayerSave(row, player); return true; }

        // Sync data.
        switch ((int) row.get("gamemode_value")) {
            case 0: player.setGameMode(GameMode.SURVIVAL); break;
            case 1: player.setGameMode(GameMode.CREATIVE); break;
            case 2: player.setGameMode(GameMode.ADVENTURE); break;
            case 3: player.setGameMode(GameMode.SPECTATOR); break;
        }

        return true;

    }


    /*
     * Implement logic to save player data.
     */
    @Override
    public boolean onPlayerSave(Row row, Player player) {

        // RET: Save disabled.
        if (!Configuration.INSTANCE.get("features").getBoolean("features.save.gameMode.enabled")) return true;

        // Save data.
        switch (player.getGameMode()) {
            case SURVIVAL: row.set("gamemode_value", 0); break;
            case CREATIVE: row.set("gamemode_value", 1); break;
            case ADVENTURE: row.set("gamemode_value", 2); break;
            case SPECTATOR: row.set("gamemode_value", 3); break;
        }
        row.update();

        return true;

    }

}
