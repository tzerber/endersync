package stackunderflow.endersync.modules;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import stackunderflow.endersync.database.mysql.Row;
import stackunderflow.endersync.utils.Configuration;


/*
 * This sync module handles the following aspects:
 * - Location
 */
@Getter
@Setter
public class LocationPlayerSyncModule extends PlayerSyncModule {

    // ================================     VARS

    // References
    // Foo


    // Settings
    // Foo




    // ================================     CONSTRUCTOR

    public LocationPlayerSyncModule() {

        // Setup SyncModule.
        super("location");

        // Setup database tables.
        getTableManager().addColumn("player_uuid", "VARCHAR(36) NOT NULL UNIQUE");
        getTableManager().addColumn("player_name", "VARCHAR(16) NOT NULL");
        getTableManager().addColumn("world", "VARCHAR(36) NOT NULL");
        getTableManager().addColumn("x", "DOUBLE NOT NULL");
        getTableManager().addColumn("y", "DOUBLE NOT NULL");
        getTableManager().addColumn("z", "DOUBLE NOT NULL");
        getTableManager().addColumn("yaw", "FLOAT NOT NULL");
        getTableManager().addColumn("pitch", "FLOAT NOT NULL");
        getTableManager().addColumn("bed_world", "VARCHAR(36) NOT NULL");
        getTableManager().addColumn("bed_x", "DOUBLE NOT NULL");
        getTableManager().addColumn("bed_y", "DOUBLE NOT NULL");
        getTableManager().addColumn("bed_z", "DOUBLE NOT NULL");
        getTableManager().addColumn("bed_yaw", "FLOAT NOT NULL");
        getTableManager().addColumn("bed_pitch", "FLOAT NOT NULL");

        // Finish the init. ! This needs to be here!
        finishModuleInit();

    }



    // ================================     LOGIC


    /*
     * Implement logic to sync player data.
     */
    @Override
    public boolean onPlayerSync(Row row, Player player) {

        // RET: Sync disabled.
        if (!Configuration.INSTANCE.get("features").getBoolean("features.sync.location.enabled")) return true;

        // RET: No entry found: Call save & Return.
        if (row.isEmptyIgnorePlayerData()) { onPlayerSave(row, player); return true; }

        // Sync data.
        if (Configuration.INSTANCE.get("features").getBoolean("features.sync.location.bedSpawn") && !((String) row.get("bed_world")).equals("null")) {
            World world = Bukkit.getWorld((String) row.get("bed_world"));
            Location target = new Location(world, (double) row.get("bed_x"), (double) row.get("bed_y"), (double) row.get("bed_z"), (float) row.get("bed_yaw"), (float) row.get("bed_pitch"));
            player.setBedSpawnLocation(target);
        }

        World world = Configuration.INSTANCE.get("features").getBoolean("features.sync.location.world") ? Bukkit.getWorld((String) row.get("world")) : player.getLocation().getWorld();
        Location target = new Location(world, (double) row.get("x"), (double) row.get("y"), (double) row.get("z"), (float) row.get("yaw"), (float) row.get("pitch"));
        player.teleport(target);

        return true;

    }


    /*
     * Implement logic to save player data.
     */
    @Override
    public boolean onPlayerSave(Row row, Player player) {

        // RET: Save disabled.
        if (!Configuration.INSTANCE.get("features").getBoolean("features.save.location.enabled")) return true;

        // Save data.
        if (Configuration.INSTANCE.get("features").getBoolean("features.save.location.world")) row.set("world", player.getLocation().getWorld().getName());
        row.set("x", player.getLocation().getX());
        row.set("y", player.getLocation().getY());
        row.set("z", player.getLocation().getZ());
        row.set("yaw", player.getLocation().getYaw());
        row.set("pitch", player.getLocation().getPitch());

        if (Configuration.INSTANCE.get("features").getBoolean("features.save.location.world") && player.getBedSpawnLocation() != null) {
            row.set("bed_world", player.getBedSpawnLocation().getWorld().getName());
            row.set("bed_x", player.getBedSpawnLocation().getX());
            row.set("bed_y", player.getBedSpawnLocation().getY());
            row.set("bed_z", player.getBedSpawnLocation().getZ());
            row.set("bed_yaw", player.getBedSpawnLocation().getYaw());
            row.set("bed_pitch", player.getBedSpawnLocation().getPitch());
        }
        else {
            row.set("bed_world", "null");
            row.set("bed_x", 0.0);
            row.set("bed_y", 0.0);
            row.set("bed_z", 0.0);
            row.set("bed_yaw", 0.0);
            row.set("bed_pitch", 0.0);
        }

        row.update();

        return true;

    }

}
