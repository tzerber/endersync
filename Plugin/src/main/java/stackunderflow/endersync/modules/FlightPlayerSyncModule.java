package stackunderflow.endersync.modules;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;
import stackunderflow.endersync.database.mysql.Row;
import stackunderflow.endersync.serializers.Serializer;
import stackunderflow.endersync.utils.Configuration;


/*
 * This sync module handles the following aspects:
 * - Flight
 */
@Getter
@Setter
public class FlightPlayerSyncModule extends PlayerSyncModule {

    // ================================     VARS

    // References
    // Foo


    // Settings
    // Foo




    // ================================     CONSTRUCTOR

    public FlightPlayerSyncModule() {

        // Setup SyncModule.
        super("flight");

        // Setup database tables.
        getTableManager().addColumn("player_uuid", "VARCHAR(36) NOT NULL UNIQUE");
        getTableManager().addColumn("player_name", "VARCHAR(16) NOT NULL");
        getTableManager().addColumn("allow_flying", "BOOLEAN NOT NULL");
        getTableManager().addColumn("flying_speed", "FLOAT NOT NULL");
        getTableManager().addColumn("is_flying", "BOOLEAN NOT NULL");

        // Finish the init. ! This needs to be here!
        finishModuleInit();

    }



    // ================================     LOGIC


    /*
     * Implement logic to sync player data.
     */
    @Override
    public boolean onPlayerSync(Row row, Player player) {

        // RET: Sync disabled.
        if (!Configuration.INSTANCE.get("features").getBoolean("features.sync.flight.enabled")) return true;

        // RET: No entry found: Call save & Return.
        if (row.isEmptyIgnorePlayerData()) { onPlayerSave(row, player); return true; }

        // Sync data.
        player.setAllowFlight(Serializer.INSTANCE.getGenericSerializer().DeserializeBoolean(row.get("allow_flying")));
        player.setFlySpeed((float) row.get("flying_speed"));
        player.setFlying(Serializer.INSTANCE.getGenericSerializer().DeserializeBoolean(row.get("is_flying")));

        return true;

    }


    /*
     * Implement logic to save player data.
     */
    @Override
    public boolean onPlayerSave(Row row, Player player) {

        // RET: Save disabled.
        if (!Configuration.INSTANCE.get("features").getBoolean("features.save.flight.enabled")) return true;

        // Save data.
        row.set("allow_flying", Serializer.INSTANCE.getGenericSerializer().SerializeBoolean(player.getAllowFlight()));
        row.set("flying_speed", player.getFlySpeed());
        row.set("is_flying", Serializer.INSTANCE.getGenericSerializer().SerializeBoolean(player.isFlying()));
        row.update();

        return true;

    }

}
