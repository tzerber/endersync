

## Main command & aliases
```
/endersync
/es
```


## Help
```
/endersync help
/es help
```


## Configuration & Management
```
/es reload
/es delete <player>
TODO: Add invsee etc. commands
```


## Manual control
```
/sync [player] - Manually starts the synchronisation of one self or another
Permissions:
    - endersync.control.sync.self
    - endersync.control.sync.others
.
.
/save [player] - Manually starts the saveing of one self or another
Permissions:
    - endersync.control.save.self
    - endersync.control.save.others
```