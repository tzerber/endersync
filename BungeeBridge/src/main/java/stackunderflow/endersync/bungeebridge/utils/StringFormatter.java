package stackunderflow.endersync.bungeebridge.utils;

import lombok.Getter;
import lombok.Setter;
import stackunderflow.endersync.bungeebridge.Main;

import java.util.List;


/*
 * Handles string formatting which can be chained.
 * See method docs for more info.
 */
@Getter
@Setter
public class StringFormatter {

    // =============  VARS

    private String STR;

    private boolean useSuccessChar = false;
    private boolean successState = false;
    private boolean progressState = false;



    // =============  CONSTRUCTOR & Return logic

    public StringFormatter(String str) {
        setSTR(str);
    }


    /*
     * Returns the final string.
     */
    public String getSTR() {

        // Replace prefix.
        this.prefix();
        return this.STR;

    }


    // =============  FORMATERS


    /*
     * Replacements:
     *      source               - with
     *
     */
    public StringFormatter replace(String source, String with) {
        if (this.STR.contains(source)) this.STR = this.STR.replace(source, with);
        return this;
    }


    /*
     * Replacements:
     *      {prefix}              - GameManager.INSTANCE.getPREFIX()
     *
     */
    public StringFormatter prefix() {

        if (this.STR.contains("{prefix}")) {

            String prefix = Main.INSTANCE.getPluginChatPrefix();
            if (prefix.contains("{success}")) {

                String successInsert = "";
                if (isUseSuccessChar()) {
                    if (isSuccessState()) {
                        successInsert = "§a§l✔§R ";
                    }
                    else if (!isSuccessState() && isProgressState()) {
                        successInsert = "§b§l...§R "; // ↻
                    }
                    else {
                        successInsert = "§c§l✘§R ";
                    }
                }
                prefix = prefix.replace("{success}", successInsert);

            }
            this.STR = this.STR.replace("{prefix}", prefix);

        }
        return this;

    }


    /*
     * Sets up the success state, so that it can later be replaced.
     *
     */
    public StringFormatter setSuccess(boolean success) {
        setUseSuccessChar(true);
        setSuccessState(success);
        return this;
    }

    public StringFormatter enableProgress() {
        setUseSuccessChar(true);
        setProgressState(true);
        return this;
    }


    /*
     * Replacements:
     *      {ownerDisplayName}      - account.getOwner()
     *      {currentBalance}        - account.getCurrentBalance()
     *      {currencySymbol}        - MoneyManager.INSTANCE.getCurrencySymbol()
     *
     */
    /*public StringFormatter playerMoneyAccount(PlayerAccount account) {

        if (this.STR.contains("{ownerDisplayName}")) this.STR = this.STR.replace("{ownerDisplayName}", account.getOwnerDisplayName());
        if (this.STR.contains("{currentBalance}")) this.STR = this.STR.replace("{currentBalance}", account.getCurrentBalanceString());
        if (this.STR.contains("{currencySymbol}")) this.STR = this.STR.replace("{currencySymbol}", MoneyManager.INSTANCE.getCurrencySymbol());

        return this;

    }*/


    /*
     * Replaces all occurrences in a string.
     */
    public static String replaceAllIfExist(String input, String replace, String with) {
        if (input.contains(replace)) input = input.replaceAll(replace, with);
        return input;
    }

}
