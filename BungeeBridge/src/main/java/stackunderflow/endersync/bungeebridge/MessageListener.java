package stackunderflow.endersync.bungeebridge;

import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import stackunderflow.endersync.bungeebridge.utils.Messaging;

import java.io.*;
import java.util.UUID;


/*
 * Handles the incoming plugin messages.
 */
public class MessageListener implements Listener {


    /*
     * Handle the message.
     */
    @EventHandler
    public void onPluginMessage(PluginMessageEvent e){
        if (e.getTag().equalsIgnoreCase("BungeeCord")) {
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(e.getData()));
            try {
                String channel = in.readUTF(); // channel we delivered

                // OP (Occupy Player)
                if (channel.equalsIgnoreCase("OP")) {
                    UUID playerUUID = UUID.fromString(in.readUTF());
                    Main.INSTANCE.logDebug("Occupying player: "+playerUUID);
                    if (!Main.INSTANCE.getPlayersOccupied().contains(playerUUID))
                        Main.INSTANCE.getPlayersOccupied().add(playerUUID);
                }


                // UOP (Un-Occupy Player)
                if (channel.equalsIgnoreCase("UOP")) {
                    UUID playerUUID = UUID.fromString(in.readUTF());
                    Main.INSTANCE.logDebug("Un-Occupying player: "+playerUUID);
                    Main.INSTANCE.getPlayersOccupied().remove(playerUUID);
                }


                // IOP (Is-Occupied Player)
                if (channel.equalsIgnoreCase("IOP")) {
                    ServerInfo server = Main.INSTANCE.getProxy().getPlayer(e.getReceiver().toString()).getServer().getInfo();
                    UUID playerUUID = UUID.fromString(in.readUTF());
                    Main.INSTANCE.logDebug("Is-Occupied player: "+playerUUID);
                    if (Main.INSTANCE.getPlayersOccupied().contains(playerUUID))
                        Messaging.sendToBukkit("IOP", playerUUID.toString()+";true", server);
                    else
                        Messaging.sendToBukkit("IOP", playerUUID.toString()+";false", server);
                }


                // SYP (Sync Player - Sync is executed on the server the player currently is on)
                if (channel.equalsIgnoreCase("SYP")) {
                    String message = in.readUTF();
                    UUID playerUUID = UUID.fromString(message.split(";")[0]);
                    ServerInfo server = Main.INSTANCE.getProxy().getPlayer(playerUUID).getServer().getInfo();
                    String moduleNames = message.substring(message.split(";")[0].length()+1, message.length());
                    Main.INSTANCE.logDebug("Sending sync request to server " + server.getName() + " for player: "+playerUUID);
                    Messaging.sendToBukkit("SYP", message, server);
                }


                // SAP (Save Player - Save is executed on the server the player currently is on)
                if (channel.equalsIgnoreCase("SAP")) {
                    String message = in.readUTF();
                    UUID playerUUID = UUID.fromString(message.split(";")[0]);
                    ServerInfo server = Main.INSTANCE.getProxy().getPlayer(playerUUID).getServer().getInfo();
                    String moduleNames = message.substring(message.split(";")[0].length()+1, message.length());
                    Main.INSTANCE.logDebug("Sending save request to server " + server.getName() + " for player: "+playerUUID);
                    Messaging.sendToBukkit("SAP", message, server);
                }


            } catch (IOException e1) {
                e1.printStackTrace();
            }

        }
    }

}
